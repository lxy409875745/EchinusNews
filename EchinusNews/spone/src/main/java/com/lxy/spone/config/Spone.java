package com.lxy.spone.config;

import android.content.Context;

import static com.lxy.spone.config.ConfigKey.APPLICATION_CONTEXT;

public final class Spone {
    public static Configurator init(Context context){
        Configurator.getInstance()
                .getSpongesConfigs()
                .put(APPLICATION_CONTEXT, context.getApplicationContext());
        return Configurator.getInstance();
    }


    public static Configurator getConfigurator() {
        return Configurator.getInstance();
    }


    public static <T> T getConfiguration(Object key) {
        return getConfigurator().getConfiguration(key);
    }


    public static Context getApplicationContext(){
        return (Context) getConfigurator().getSpongesConfigs().get(ConfigKey.APPLICATION_CONTEXT);
    }


}
