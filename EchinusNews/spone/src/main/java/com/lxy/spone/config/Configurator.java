package com.lxy.spone.config;

import java.util.HashMap;

public class Configurator {

    private static final HashMap<Object, Object> SPONGE_CONFIGS = new HashMap<>();


    public Configurator() {
        SPONGE_CONFIGS.put(ConfigKey.CONFIG_READY,true);
    }

    private static final class Holder {
        private static final Configurator INSTANCE = new Configurator();
    }

    public static Configurator getInstance(){
        return Holder.INSTANCE;
    }


    public final HashMap<Object, Object> getSpongesConfigs() {
        return SPONGE_CONFIGS;
    }

    private void checkConfiguration() {
        final boolean isReady = (boolean) SPONGE_CONFIGS.get(ConfigKey.CONFIG_READY);
        if (!isReady) {
            throw new RuntimeException("Configuration is not ready,call configure");
        }
    }

    @SuppressWarnings("unchecked")
    final <T> T getConfiguration(Object key) {
        checkConfiguration();
        final Object value = SPONGE_CONFIGS.get(key);
        if (value == null) {
            throw new NullPointerException(key.toString() + " IS NULL");
        }
        return (T) SPONGE_CONFIGS.get(key);
    }

}
