package com.wavpayment.echinusnews.ui.main.news.newslist;

import android.content.Context;
import android.support.annotation.NonNull;

import com.lzy.okgo.db.DBUtils;
import com.wavpayment.echinusnews.comment.callback.CallbackManager;

import com.wavpayment.echinusnews.comment.db.DBManager;
import com.wavpayment.echinusnews.comment.db.NewsBeanDao;
import com.wavpayment.echinusnews.comment.network.NetworkHandler;
import com.wavpayment.echinusnews.comment.network.URLManager;
import com.wavpayment.echinusnews.entity.NewsBean;
import com.wavpayment.echinusnews.entity.NewsTypeBean;

import java.util.Iterator;
import java.util.List;

/**
 * Created by lenovo on 2018/5/21.
 */

public abstract class NewslIstModle {
    private Context mContext;

    public NewslIstModle(Context context, NewsTypeBean typeBean) {
        initCallback(typeBean);
        mContext = context;
    }

    private URLManager.Type newsType;

    private void initCallback(NewsTypeBean typeBean) {
        CallbackManager.getInstance().addCallback(newsType = URLManager.Type.containsUrl2Type(typeBean.getUrl()), values -> netRequestCallback(values));
    }


    public void netRequestData(int nowPage, int pageNumber) {
        NetworkHandler.getNews(newsType, nowPage, pageNumber);
    }


    public void localRequestData(int nowPage, int pageNumber) {
        String type = URLManager.Type.containsType2Url(newsType);
        List<NewsBean> localData = queryNewsBeanByNewsType(type, nowPage, pageNumber);
        localRequestCallback(localData);

    }


    public abstract void netRequestCallback(Object value);

    public abstract void localRequestCallback(Object value);


    //---------------------------------------------DB操作---------------------------------------------------

    //插入一條數據
    public void insertData(NewsBean newsBean) {
        DBManager.getDaoSession(mContext).getNewsBeanDao().insert(newsBean);
    }

    //插入一套數據
    public void insertData(List<NewsBean> newsBeans) {
        Iterable<NewsBean> iterable = () -> newsBeans.iterator();
        DBManager.getDaoSession(mContext).getNewsBeanDao().insertInTx(iterable);
    }

    public void deleteAll() {
        DBManager.getDaoSession(mContext).getNewsBeanDao().deleteAll();
    }

    //刪除某一分類的新聞
    public void deleteByNewsType(String newsType) {
        DBManager.getDaoSession(mContext).getNewsBeanDao().queryBuilder()
                .where(NewsBeanDao.Properties.NewsType
                        .eq(newsType)).buildDelete()
                .executeDeleteWithoutDetachingEntities();
    }

    //獲取某個分類中 最開始的那一條數據
    public List<NewsBean> queryNewsBeanByRequestTime(String newsType) {
        //select * from om_meeting_schedule s where s.is_use=1
        //ORDER BY ABS(NOW() - s.meeting_begin_date) ASC
        //limit 1
        List<NewsBean> newsBeans = DBManager.getDaoSession(mContext).getNewsBeanDao().queryBuilder()
                .where(NewsBeanDao.Properties.NewsType
                        .eq(newsType))
                .orderAsc(NewsBeanDao.Properties.RequestTime)
                .limit(1)
                .list();
        return newsBeans;
    }

    //獲取分類數據
    public List<NewsBean> queryNewsBeanByNewsType(String newsType, int nowPage, int pageNumber) {
        List<NewsBean> newsBeans = DBManager.getDaoSession(mContext).getNewsBeanDao().queryBuilder()
                .where(NewsBeanDao.Properties.NewsType
                        .eq(newsType))
                .offset(nowPage * pageNumber)
                .limit(pageNumber)
                .list();
        return newsBeans;
    }

    //獲取某個分類的數量
    public long queryNewsBeanNewsTypeCount(String newsType) {
        long count = DBManager.getDaoSession(mContext).getNewsBeanDao().queryBuilder()
                .where(NewsBeanDao.Properties.NewsType
                        .eq(newsType))
                .buildCount().count();
        return count;
    }


}
