package com.wavpayment.echinusnews.comment.network;

import android.util.Log;

import com.google.gson.JsonObject;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.wavpayment.echinusnews.adapter.NewsListViewAdapter;
import com.wavpayment.echinusnews.adapter.SearchNewsAdapter;
import com.wavpayment.echinusnews.adapter.WechatListAdapter;
import com.wavpayment.echinusnews.comment.callback.CallbackConfig;
import com.wavpayment.echinusnews.comment.callback.CallbackManager;
import com.wavpayment.echinusnews.entity.NewsBean;
import com.wavpayment.echinusnews.entity.NewsResultBean;
import com.wavpayment.echinusnews.entity.NewsTypeBean;
import com.wavpayment.echinusnews.other.JsonReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Administrator on 2018/5/12.
 */

public class NetworkHandler {


    public static void getNews(URLManager.Type newsType, int nowPage, int pageNumber) {
        HashMap<String, String> params = new HashMap<>();
        params.put("key", URLManager.TIANXING_API_KEY);
        params.put("num", pageNumber + "");
        params.put("page", nowPage + "");
        String newsTypeString = URLManager.Type.containsType2Url(newsType);
        Network.getInstanse()
                .get(URLManager.BASE_URL + newsTypeString, params)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        if (isJSONValid(response.body())) {
                            NewsResultBean resultBean = JsonReader.getInstanse().readNews(response.body());
                            if (resultBean != null) {
                                NewsBean tempNewsBean;
                                for (int i = 0; i < resultBean.getNewslist().size(); i++) {
                                    tempNewsBean = resultBean.getNewslist().get(i);
                                    tempNewsBean.setNewsType(newsTypeString);
                                    if (nowPage == 1){ //第一页
                                        if (i == 0 || i == 1 || i == 2) {
                                            tempNewsBean.setItemType(NewsListViewAdapter.ITEM_TYPE_BANNER);
                                        } if (i % 2 == 0) {
                                            tempNewsBean.setItemType(NewsListViewAdapter.ITEM_TYPE_NORMAL);
                                        } else if (i % 3 == 0) {
                                            tempNewsBean.setItemType(NewsListViewAdapter.ITEM_TYPE_BIG);
                                        } else {
                                            tempNewsBean.setItemType(NewsListViewAdapter.ITEM_TYPE_NORMAL);
                                        }

                                    }else {
                                        if (i == 0) {
                                            tempNewsBean.setItemType(NewsListViewAdapter.ITEM_TYPE_NORMAL);
                                        }if (i % 2 == 0) {
                                            tempNewsBean.setItemType(NewsListViewAdapter.ITEM_TYPE_NORMAL);
                                        } else if (i % 3 == 0) {
                                            tempNewsBean.setItemType(NewsListViewAdapter.ITEM_TYPE_BIG);
                                        } else {
                                            tempNewsBean.setItemType(NewsListViewAdapter.ITEM_TYPE_NORMAL);
                                        }

                                    }
                                }

                                CallbackManager.getInstance().getCallback(newsType).excuteCallback(resultBean);
                            } else {
                                CallbackManager.getInstance().getCallback(newsType).excuteCallback(null);
                            }
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        CallbackManager.getInstance().getCallback(newsType).excuteCallback(null);
                    }
                });
    }


    /**
     * 获取微信文章
     *
     * @param nowPage
     * @param pageNumber
     */
    public static void getWechatNews(int nowPage, int pageNumber) {
        HashMap<String, String> params = new HashMap<>();
        params.put("key", URLManager.TIANXING_API_KEY);
        params.put("num", pageNumber + "");
        params.put("page", nowPage + "");
        Network.getInstanse()
                .get(URLManager.BASE_URL + URLManager.WECHAT_URL, params)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        if (isJSONValid(response.body())) {
                            if (response.code() == 200){
                                NewsResultBean resultBean = JsonReader.getInstanse().readNews(response.body());
                                if (resultBean != null) {
                                    NewsBean tempNewsBean;
                                    for (int i = 0; i < resultBean.getNewslist().size(); i++) {
                                        tempNewsBean = resultBean.getNewslist().get(i);
                                        tempNewsBean.setNewsType(URLManager.WECHAT_URL); //每个都必须设置新闻类别
                                        if ((i == 0) || (i == 10) || ( i== 7) || i== 17) {
                                            tempNewsBean.setItemType(WechatListAdapter.ITEM_TYPE_BIG);
                                        } else {
                                            tempNewsBean.setItemType(WechatListAdapter.ITEM_TYPE_NORMAL);
                                        }
                                    }
                                    CallbackManager.getInstance().getCallback(CallbackConfig.WECHAT_NEWS_NET).excuteCallback(resultBean);
                                } else {
                                    CallbackManager.getInstance().getCallback(CallbackConfig.WECHAT_NEWS_NET).excuteCallback(null);
                                }
                            }else {
                                CallbackManager.getInstance().getCallback(CallbackConfig.WECHAT_NEWS_NET).excuteCallback(null);
                            }
                        }else{
                            CallbackManager.getInstance().getCallback(CallbackConfig.WECHAT_NEWS_NET).excuteCallback(null);
                        }
                    }


                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        CallbackManager.getInstance().getCallback(CallbackConfig.WECHAT_NEWS_NET).excuteCallback(null);
                    }
                });
    }


    /**
     * 查找根據搜索的關鍵字 獲取對應的新聞
     *
     * @param nowPage
     * @param pageNumber
     * @param keyword
     */
    public static void getNewsByKeyWord(NewsTypeBean newsTypeBean, int nowPage, int pageNumber, String keyword) {
        URLManager.Type newsType = URLManager.Type.containsUrl2Type(newsTypeBean.getUrl());
        HashMap<String, String> params = new HashMap<>();
        params.put("key", URLManager.TIANXING_API_KEY);
        params.put("num", pageNumber + "");
        params.put("page", nowPage + "");
        params.put("word", keyword);
        String newsTypeString = URLManager.Type.containsType2Url(newsType);
        Network.getInstanse()
                .get(URLManager.BASE_URL + newsTypeString, params)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        if (isJSONValid(response.body())) {
                            NewsResultBean resultBean = JsonReader.getInstanse().readNews(response.body());
                            if (resultBean != null) {
                                CallbackManager.getInstance().getCallback(newsType).excuteCallback(resultBean);
                            } else {
                                CallbackManager.getInstance().getCallback(newsType).excuteCallback(null);
                            }
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        CallbackManager.getInstance().getCallback(newsType).excuteCallback(null);
                    }
                });

    }


    /**
     * 获取推荐新闻  （随机的社会新闻）
     */
    public static void getRecommendNews() {
        HashMap<String, String> params = new HashMap<>();
        params.put("key", URLManager.TIANXING_API_KEY);
        params.put("num", 10 + "");//直接加载10条新闻
        params.put("page", 1 + "");
        params.put("rand", 1 + ""); //设置成随机
        Network.getInstanse()
                .get(URLManager.BASE_URL + URLManager.NEWS_SOCIAL, params)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        if (isJSONValid(response.body())) {
                            NewsResultBean resultBean = JsonReader.getInstanse().readNews(response.body());
                            if (resultBean != null) {
                                CallbackManager.getInstance().getCallback(CallbackConfig.RECOMMEND_NEWS).excuteCallback(resultBean);
                            } else {
                                CallbackManager.getInstance().getCallback(CallbackConfig.RECOMMEND_NEWS).excuteCallback(null);
                            }
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        CallbackManager.getInstance().getCallback(CallbackConfig.RECOMMEND_NEWS).excuteCallback(null);
                    }
                });

    }


    //判斷是否是Json數據
    public final static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
            return true;
        } catch (JSONException e) {
            try {
                new JSONArray(test);
                return true;
            } catch (JSONException e1) {
                return false;
            }
        }
    }
}
