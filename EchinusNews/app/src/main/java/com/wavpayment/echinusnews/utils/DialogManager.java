package com.wavpayment.echinusnews.utils;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.vondear.rxui.view.dialog.RxDialogLoading;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.app.EchinusNewsApplication;

import java.util.logging.Handler;

public class DialogManager {
    private RxDialogLoading mDialogLoading;//加载dialog

    private DialogManager() {
    }

    private static class Handler{
        private static final DialogManager INSTANCE = new DialogManager();
    }


    public static DialogManager getInstance(){
        return Handler.INSTANCE;
    }
    private Context mContext;
    public void init(Context context){
        mContext = context;
    }


    public void showLoading(){
        if (mDialogLoading == null){
            mDialogLoading = new RxDialogLoading(mContext);
        }
        showLoading(true);
    }

    /**
     *
     * @param canCancel 是否可以被取消
     */
    public void showLoading(boolean canCancel){
        if (mDialogLoading == null){
            mDialogLoading = new RxDialogLoading(mContext);
        }
        showLoading(null,canCancel);
    }

    /**
     * @param loadingText 加载时的文字
     * @param canCancel
     */
    public void showLoading(String loadingText,boolean canCancel){
        if (mDialogLoading == null){
            mDialogLoading = new RxDialogLoading(mContext);
        }
        mDialogLoading.setCancelable(canCancel);
        mDialogLoading.setLoadingColor(ContextCompat.getColor(mContext, R.color.app_color));
        mDialogLoading.setLoadingText(loadingText);
        mDialogLoading.show();
    }



    public void stopLoading(){
        if (mDialogLoading != null){
            mDialogLoading.dismiss();
        }
    }


    /**
     * 创建一个搜索新闻搜索历史记录popupwindow
     * @param context
     * @return
     */
    public PopupWindow creatSearchHistoryWindow(Context context){
        View contentView = LayoutInflater.from(context).inflate(R.layout.popup_search_news_history,null);
        PopupWindow popupWindow = new PopupWindow();


        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);  //当popup失去焦点或者点击空白是否会dissmiss
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // 设置SelectPicPopupWindow弹出窗体的宽
        popupWindow.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体的高
        popupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(contentView);
        return popupWindow;
    }

}
