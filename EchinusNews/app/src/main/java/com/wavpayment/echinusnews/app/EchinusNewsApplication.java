package com.wavpayment.echinusnews.app;

import android.content.Context;

import com.lxy.spone.config.ConfigKey;
import com.lxy.spone.config.Spone;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheEntity;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.https.HttpsUtils;
import com.lzy.okgo.interceptor.HttpLoggingInterceptor;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.tencent.bugly.crashreport.CrashReport;
import com.vondear.rxtool.RxTool;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.base.BaseApplication;
import com.wavpayment.echinusnews.utils.NetUtils;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import okhttp3.OkHttpClient;

public class EchinusNewsApplication extends BaseApplication {
    private final String BUGLY_APPID = "2b02f219af";// bugly的APPID

    private static EchinusNewsApplication INSTANCE;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        initRxTools();
        initSpone();
        initRefreshLayout();
        initOkGo();
        initBugly();
    }

    private void initBugly() {
        CrashReport.initCrashReport(getApplicationContext(), BUGLY_APPID, true);//最后一个为调试开关  建议在测试阶段建议设置成true，发布时设置为false。
    }


    public static EchinusNewsApplication getInstance(){
        return INSTANCE;
    }


    private void initRxTools() {
        RxTool.init(this);
    }

    private void initSpone() {
        Spone.init(this);
        //初始化网络请求
        if (NetUtils.isNetworkConnected(this)){
            if (NetUtils.isWifiConnected(this)){
                Spone.getConfigurator().getSpongesConfigs().put(ConfigKey.NET_STATUS,0);
            }else {
                Spone.getConfigurator().getSpongesConfigs().put(ConfigKey.NET_STATUS,1);
            }
        }else {
            Spone.getConfigurator().getSpongesConfigs().put(ConfigKey.NET_STATUS,2);
        }
    }


    private void initRefreshLayout() {
        //设置全局的Header构建器
        SmartRefreshLayout.setDefaultRefreshHeaderCreator((context, layout) -> {
            layout.setPrimaryColorsId(R.color.color_WhiteE, R.color.color_gray80);//全局设置主题颜色
            ClassicsHeader classicsHeader = new ClassicsHeader(context);
            return classicsHeader;//.setTimeFormat(new DynamicTimeFormat("更新于 %s"));//指定为经典Header，默认是 贝塞尔雷达Header
        });


    }


    private void initOkGo() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        //log相关
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor("OkGo");
        loggingInterceptor.setPrintLevel(HttpLoggingInterceptor.Level.BODY);        //log打印级别，决定了log显示的详细程度
        loggingInterceptor.setColorLevel(Level.INFO);                               //log颜色级别，决定了log在控制台显示的颜色
        builder.addInterceptor(loggingInterceptor);                                 //添加OkGo默认debug日志

        //超时时间设置，默认60秒
        builder.readTimeout(OkGo.DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);      //全局的读取超时时间
        builder.writeTimeout(OkGo.DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);     //全局的写入超时时间
        builder.connectTimeout(OkGo.DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);   //全局的连接超时时间

        HttpsUtils.SSLParams sslParams1 = HttpsUtils.getSslSocketFactory();
        builder.sslSocketFactory(sslParams1.sSLSocketFactory, sslParams1.trustManager);

        OkGo.getInstance().init(this)                           //必须调用初始化
                .setOkHttpClient(builder.build())               //建议设置OkHttpClient，不设置会使用默认的
                .setCacheMode(CacheMode.NO_CACHE)               //全局统一缓存模式，默认不使用缓存，可以不传
                .setCacheTime(CacheEntity.CACHE_NEVER_EXPIRE)   //全局统一缓存时间，默认永不过期，可以不传
                .setRetryCount(3);                            //全局统一超时重连次数，默认为三次，那么最差的情况会请求4次(一次原始请求，三次重连请求)，不需要可以设置为0

    }




}
