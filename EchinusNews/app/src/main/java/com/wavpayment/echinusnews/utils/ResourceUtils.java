package com.wavpayment.echinusnews.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

/**
 * Created by Administrator on 2018/5/11.
 */

public class ResourceUtils {

    public static int getColor(Context context,@ColorRes int colorRes){
       return ContextCompat.getColor(context,colorRes);
    }


    public static Drawable getDrawable(Context context, @DrawableRes int drawableRes){
        return ContextCompat.getDrawable(context,drawableRes);
    }


}
