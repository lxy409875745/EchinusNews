package com.wavpayment.echinusnews.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.entity.NewsBean;

import java.util.List;

public class WechatListAdapter extends BaseMultiItemQuickAdapter<NewsBean, WechatListAdapter.WechatListHolder> {

    public static final int ITEM_TYPE_NORMAL = 0x00000000;
    public static final int ITEM_TYPE_BIG = 0x00000001;

    private Context mContext;
    private RequestOptions mImageOption;
    private DrawableCrossFadeFactory drawableCrossFadeFactory;

    public WechatListAdapter(Context context) {
        super(null);
        mContext = context;
        //設置好Glide選項
        mImageOption = new RequestOptions();
        mImageOption.error(R.drawable.error_img);
        mImageOption.placeholder(R.drawable.loading_img);
        //圖片加載動畫
        drawableCrossFadeFactory = new DrawableCrossFadeFactory.Builder(300).setCrossFadeEnabled(true).build();
        addItemType(ITEM_TYPE_NORMAL, R.layout.item_wechat_normal);
        addItemType(ITEM_TYPE_BIG, R.layout.item_wechat_big);
    }

    @Override
    protected void convert(WechatListHolder helper, NewsBean item) {
        int itemType = helper.getItemViewType();
        switch (itemType) {
            case ITEM_TYPE_NORMAL:
                helper.setText(R.id.item_wechat_normal_title_tv, item.getTitle());
                ImageView normalImg = helper.getView(R.id.item_wechat_normal_img);
                Glide.with(mContext)
                        .applyDefaultRequestOptions(mImageOption)
                        .load(item.getPicUrl())
                        .transition(DrawableTransitionOptions.with(drawableCrossFadeFactory))
                        .into(normalImg);
                break;
            case ITEM_TYPE_BIG:
                helper.setText(R.id.item_wechat_big_title, item.getTitle());
                ImageView bigImg = helper.getView(R.id.item_wechat_big_img);
                Glide.with(mContext)
                        .applyDefaultRequestOptions(mImageOption)
                        .load(item.getPicUrl())
                        .transition(DrawableTransitionOptions.with(drawableCrossFadeFactory))
                        .into(bigImg);
                break;
        }
    }

    public class WechatListHolder extends BaseViewHolder {

        public WechatListHolder(View view) {
            super(view);
        }
    }
}
