package com.wavpayment.echinusnews.ui.main.news.newslist;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lxy.spone.config.Spone;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.adapter.NewsListViewAdapter;
import com.wavpayment.echinusnews.base.BaseFragmentDelegate;
import com.wavpayment.echinusnews.base.BaseFragmentEventBusDelegate;
import com.wavpayment.echinusnews.base.BaseView;
import com.wavpayment.echinusnews.comment.anim.SharedElementTransation;
import com.wavpayment.echinusnews.comment.callback.EventBusPosKey;
import com.wavpayment.echinusnews.entity.NewsTypeBean;
import com.wavpayment.echinusnews.ui.web.Html5WebDelegate;
import com.wavpayment.echinusnews.utils.NetUtils;
import com.wavpayment.echinusnews.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Administrator on 2018/5/12.
 */

public class NewsListDelegate extends BaseFragmentEventBusDelegate implements BaseView {
    private static final String TAG = "NewsListDelegate";
    private boolean isFristLoad = true;
    private BaseFragmentDelegate mParentFragmentDelegate;//父容器的fragment 用于打开新页面

    private NewsTypeBean mTypeBean;
    //加载和错误显示
    private ImageView loadingImageView;
    private View loadingContainer;
    private TextView loadingTextView;

    private int lastOffset = 0;//recyclerview 顶部view的偏移量
    private int lastPosition = 0;//得到view的位置
    private boolean isVisible = false; //当前页面是否是显示的状态


    @Override
    public Object setLayout() {
        return R.layout.fragment_newslist;
    }

    @Override
    public void onBindeView(View rootView, @Nullable Bundle savedInstanceState) {
        initTypeData();
        initLoadingView();
        initRecylerview();
        initRefreshView();
        initPresenter();
        initData();
    }


    private void initLoadingView() {
        loadingContainer = bind(R.id.fragment_newslist_loading_container);
        loadingImageView = bind(R.id.fragment_newslist_loading_imageView);
        loadingTextView = bind(R.id.fragment_newslist_error_textView);
        loadingContainer.setOnClickListener(v -> {
            if (loadingTextView.getVisibility() == View.VISIBLE) { //只有错误提示显示的时候才能点击
                Glide.with(getContext()).asGif().load(R.mipmap.loading).into(loadingImageView);//显示加载
                loadingTextView.setVisibility(View.INVISIBLE);
                loadingContainer.setVisibility(View.VISIBLE);
                refresh();//刷新
            }
        });
    }

    /**
     * 懒加载
     *
     * @param savedInstanceState
     */
    //懒加载
    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);
        // initData();
        Log.i(TAG, "onLazyInitView: 懒加载");

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initData() { //懒加载数据
        if (isFristLoad) {
            refresh();
            // mRefreshLayout.autoRefresh();
        } else {
            if (mAdapter.getData().size() == 0) {  //当前adapter没数据
                Glide.with(getContext()).asGif().load(R.mipmap.loading).into(loadingImageView);
                loadingTextView.setVisibility(View.INVISIBLE);
                loadingContainer.setVisibility(View.VISIBLE);
                refresh(); //直接加载
            }
        }

    }


    private NewsListPresenter mPresenter;

    private void initPresenter() {
        mPresenter = new NewsListPresenter(getContext(), this, mAdapter, mTypeBean);
    }

    private void initTypeData() {
        mTypeBean = getArguments().getParcelable("newsType");
    }

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private NewsListViewAdapter mAdapter;

    private void initRecylerview() {
        mRecyclerView = bind(R.id.fragment_newslist_recy);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new NewsListViewAdapter(getContext());

        mAdapter.setOnItemClickListener((adapter, view, position) -> {//--------------------------------item点击
            Log.i(TAG, "initRecylerview: 点击事件 position = " + position);
            EventBus.getDefault().post(EventBusPosKey.HIDE_BOTTOM_NAVIGATIONBAR);
            EventBus.getDefault().post(EventBusPosKey.HIDE_BOTTOM_TO_TOP_BOTTON);
            Html5WebDelegate webDelegate = Html5WebDelegate.creat(mAdapter.getData().get(position));
            //使用进栈的父容器打开    暂无效果
            View titleView = mAdapter.getViewByPosition(position, R.id.item_news_tv_title);
            View imgView = mAdapter.getViewByPosition(position, R.id.item_news_img);
         if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                SharedElementTransation sharedElementTransation = new SharedElementTransation();
                mParentFragmentDelegate.setExitTransition(new Fade());
                webDelegate.setEnterTransition(new Fade());
                webDelegate.setSharedElementReturnTransition(sharedElementTransation);
                webDelegate.setSharedElementEnterTransition(sharedElementTransation);

                // 25.1.0以下的support包,Material过渡动画只有在进栈时有,返回时没有;
                // 25.1.0+的support包，SharedElement正常
                mParentFragmentDelegate
                        .extraTransaction()
                        .addSharedElement(titleView, getString(R.string.transation_news_title_text))
                        .addSharedElement(imgView, getString(R.string.transation_news_head_img))
                        .start(webDelegate);
            } else {
                mParentFragmentDelegate.getSupportDelegate().start(webDelegate);
            }
        });

        //监听RecyclerView滚动状态
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {//当前滚动的距离
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (recyclerView.getLayoutManager() != null) {
                    getPositionAndOffset();
                }
            }
        });

        mRecyclerView.setAdapter(mAdapter);
        //dy > 0 时为手指向上滚动,列表滚动显示下面的内容
        //dy < 0 时为手指向下滚动,列表滚动显示上面的内容
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {//向上滑動 隱藏菜單  判断底部菜单是否显示
                    //Log.i(TAG,"向上滑动");
                    EventBus.getDefault().post(EventBusPosKey.HIDE_BOTTOM_NAVIGATIONBAR);
                    EventBus.getDefault().post(EventBusPosKey.SHOW_BOTTOM_TO_TOP_BOTTON);
                } else if (dy < -40) {//向下滑動顯示菜單
                    // Log.i(TAG,"向下滑动");
                    EventBus.getDefault().post(EventBusPosKey.SHOW_BOTTOM_NAVIGATIONBAR);
                    EventBus.getDefault().post(EventBusPosKey.HIDE_BOTTOM_TO_TOP_BOTTON);

                }
            }
        });

        mAdapter.setOnLoadMoreListener(() -> loadMore(), mRecyclerView);
    }

    private SmartRefreshLayout mRefreshLayout;

    private void initRefreshView() {
        mRefreshLayout = bind(R.id.fragment_newslist_refresh);
        mRefreshLayout.setOnRefreshListener(refreshLayout -> refresh());

    }


    //---------------------------------------------------------------------------刷新加载--------------------------------

    /**
     * 直接加载
     */
    @Override
    public void refresh() {
        mPresenter.refresh();
    }

    @Override
    public void loadMore() {
        mPresenter.loadMore();
    }

    @Override
    public void refreshDone() {
        loadingContainer.setVisibility(View.INVISIBLE);

        if (NetUtils.isNetworkConnected(getContext())) {
            mRefreshLayout.finishRefresh(true);
        } else {
            showLoadErrorToast();
            mRefreshLayout.finishRefresh(false);//没网的时候还是刷新失败
        }
        if (isFristLoad) {
            isFristLoad = false;
        }
    }

    @Override
    public void refreshError(Exception e) {
        if (mAdapter.getData().size() == 0) {
            Glide.with(getContext()).asGif().load(R.drawable.ic_error_gray9_24dp).into(loadingImageView);
            loadingTextView.setVisibility(View.VISIBLE);
            loadingContainer.setVisibility(View.VISIBLE);
        }
        mRefreshLayout.finishRefresh(false);
        showLoadErrorToast();
    }

    @Override
    public void loadMoreDone(boolean isNoMoreData) {
        if (isNoMoreData) {
            mRefreshLayout.setNoMoreData(true);
            mAdapter.loadMoreEnd();
        } else {
            mRefreshLayout.setNoMoreData(false);
            mAdapter.loadMoreComplete();
        }
    }

    @Override
    public void loadError(Exception e) {
        mAdapter.loadMoreFail();
        showLoadErrorToast();
    }

    private void showLoadErrorToast() {
        if (!NetUtils.isNetworkConnected(Spone.getApplicationContext())) {
            //ToastUtils.warning(getString(R.string.comment_wraning_net_disconnect));    暂时去掉网络断掉的toast
        } else {
            ToastUtils.warning(getString(R.string.newslist_wraning_load_data_error));
        }
    }


    //----------------------------------------------------------------------------滚动操作-----------------------

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
        } else {
            isVisible = false;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void scorllTopEvent(EventBusPosKey key) {
        if (key == EventBusPosKey.SCROLL_TO_TOP) {
            if (mRecyclerView != null && isVisible) {
                mRecyclerView.scrollToPosition(0);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: ");
        getPositionAndOffset();
    }

    /**
     * 记录RecyclerView当前位置
     */

    private boolean isShow = false;

    private void getPositionAndOffset() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        //获取可视的第一个view
        View topView = layoutManager.getChildAt(0);
        if (topView != null) {
            //获取与该view的顶部的偏移量
            lastOffset = topView.getTop();
            //得到该View的数组位置
            lastPosition = layoutManager.getPosition(topView);
            Log.i(TAG, "getPositionAndOffset: 当前的偏移量：" + lastOffset + "    位置：" + lastPosition);

            if (lastPosition >= 2) { //当前移动位置超过两个单位，显示返回最顶按钮
                if (!isShow) {
                    // EventBus.getDefault().post(EventBusPosKey.SHOW_BOTTOM_TO_TOP_BOTTON);
                    isShow = true;
                }
            } else {
                if (isShow) {
                    isShow = false;
                    // EventBus.getDefault().post(EventBusPosKey.HIDE_BOTTOM_TO_TOP_BOTTON);
                }
            }
        }
    }


    //---------------------------------------------------------------对外公布的方法

    /**
     * 设置父容器的fragmnet  打开新页面的操作必须用父容器的fragment打开 因为当前的fragment是平级的fragmnet 没有进栈，如果直接打开则会使得fragmentmanager的栈混乱
     */
    public void setParantDelegateFragment(BaseFragmentDelegate parentFragmentDelegate) {
        mParentFragmentDelegate = parentFragmentDelegate;

    }
}

