package com.wavpayment.echinusnews.base;


import android.os.Bundle;
import android.support.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;

public abstract class BaseQuitDoubleClickDelegate extends BaseFragmentDelegate {

    // 再点一次退出程序时间设置
    private static final long WAIT_TIME = 2000L;
    private long TOUCH_TIME = 0;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    protected void checkQuit(){
        if (System.currentTimeMillis() - TOUCH_TIME < WAIT_TIME) {
            quit();
        } else {
            TOUCH_TIME = System.currentTimeMillis();
            notQuit();
        }

    }

    //退出
    public abstract void quit();
    //不退出
    public abstract void notQuit();


    @Override
    public boolean onBackPressedSupport() {
        checkQuit();
        return true;
    }

}
