package com.wavpayment.echinusnews.comment.network;

/**
 * Created by Administrator on 2018/5/12.
 */


public class URLManager{
    public static final String BASE_URL = "http://api.tianapi.com";
    public static final String TIANXING_API_KEY = "691da03ad94dc03a3e97bf4674eb70ca";
    //-------------------------------------------------------------------------------------------微信----------------------------------------------------------
    public static final String WECHAT_URL = "/wxnew/";//微信文章


    //-------------------------------------------------------------------------------------------新闻-------------------------------------------------------------
    public static final String NEWS_SOCIAL = "/social/"; //社会新闻API接口服务
    public static final String NEWS_GUONEI = "/guonei/";//国内新闻API接口服务
    public static final String NEWS_WORLD = "/world/";//国际新闻API接口服务
    public static final String NEWS_HUABIAN = "/huabian/";//娱乐新闻、明星花边、探班、娱乐活动等
    public static final String NEWS_TIYU = "/tiyu/"; //国内体育行业、体育明星动态等
    public static final String NEWS_NBA = "/nba/";//NBA动态、篮球赛等
    public static final String NEWS_FOOTBALL = "/football/";//国足资讯、国足明星动态等
    public static final String NEWS_KEJI = "/keji/";//信息科技、数码科技、物理科技
    public static final String NEWS_STARTUP = "/startup/"; //互联网创业、创新、创业人物动态
    public static final String NEWS_APPLE = "/apple/";//Apple产品动态，果粉、教程帮助
    public static final String NEWS_MILITARY = "/military/";//军事资讯、军情动态、科技发展等
    public static final String NEWS_MOBILE = "/mobile/";//移动互联网行业资讯
    public static final String NEWS_TRAVEL = "/travel/";//旅游、周边、景点
    public static final String NEWS_HEALTH = "/health/";//健康知识、养生、中西医
    public static final String NEWS_QIWEN = "/qiwen/";//世界奇闻、民间趣事、灵异事件等
    public static final String NEWS_MEINV = "/meinv/";//美女图片、大家都懂
    public static final String NEWS_VR = "/vr/";//VR虚拟现实相关新闻资讯
    public static final String NEWS_IT = "/it/";//IT行业相关新闻资讯





    public  enum Type {
        NEWS_SOCIAL,
        NEWS_GUONEI ,
        NEWS_WORLD ,
        NEWS_HUABIAN ,
        NEWS_TIYU,
        NEWS_NBA ,
        NEWS_FOOTBALL ,
        NEWS_KEJI,
        NEWS_STARTUP,
        NEWS_APPLE,
        NEWS_MILITARY,
        NEWS_MOBILE,
        NEWS_TRAVEL,
        NEWS_HEALTH,
        NEWS_QIWEN,
        NEWS_MEINV,
        NEWS_VR,
        NEWS_IT;

        public static final String containsType2Url(Type type){
            switch (type){
                case NEWS_SOCIAL:
                    return URLManager.NEWS_SOCIAL;
                case NEWS_GUONEI:
                    return URLManager.NEWS_GUONEI;
                case NEWS_WORLD:
                    return URLManager.NEWS_WORLD;
                case NEWS_HUABIAN:
                    return URLManager.NEWS_HUABIAN;
                case NEWS_TIYU:
                    return URLManager.NEWS_TIYU;
                case NEWS_NBA:
                    return URLManager.NEWS_NBA;
                case NEWS_FOOTBALL:
                    return URLManager.NEWS_FOOTBALL;
                case NEWS_KEJI:
                    return URLManager.NEWS_KEJI;
                case NEWS_STARTUP:
                    return URLManager.NEWS_STARTUP;
                case NEWS_APPLE:
                    return URLManager.NEWS_APPLE;
                case NEWS_MILITARY:
                    return URLManager.NEWS_MILITARY;
                case NEWS_MOBILE:
                    return URLManager.NEWS_MOBILE;
                case NEWS_TRAVEL:
                    return URLManager.NEWS_TRAVEL;
                case NEWS_HEALTH:
                    return URLManager.NEWS_HEALTH;
                case NEWS_QIWEN:
                    return URLManager.NEWS_QIWEN;
                case NEWS_MEINV:
                    return URLManager.NEWS_MEINV;
                case NEWS_VR:
                    return URLManager.NEWS_VR;
                case NEWS_IT:
                    return URLManager.NEWS_IT;
                default:return null;
            }
        }


        public static final Type containsUrl2Type(String url){
            switch (url){
                case URLManager.NEWS_SOCIAL:
                    return NEWS_SOCIAL;
                case URLManager.NEWS_GUONEI :
                    return NEWS_GUONEI;
                case URLManager.NEWS_WORLD:
                    return  NEWS_WORLD;
                case URLManager.NEWS_HUABIAN:
                    return NEWS_HUABIAN;
                case URLManager.NEWS_TIYU:
                    return  NEWS_TIYU;
                case URLManager.NEWS_NBA:
                    return  NEWS_NBA;
                case URLManager.NEWS_FOOTBALL:
                    return  NEWS_FOOTBALL;
                case URLManager.NEWS_KEJI:
                    return  NEWS_KEJI;
                case URLManager.NEWS_STARTUP:
                    return  NEWS_STARTUP;
                case URLManager.NEWS_APPLE:
                    return  NEWS_APPLE;
                case URLManager.NEWS_MILITARY:
                    return  NEWS_MILITARY;
                case URLManager.NEWS_MOBILE:
                    return  NEWS_MOBILE;
                case URLManager.NEWS_TRAVEL:
                    return  NEWS_TRAVEL;
                case URLManager.NEWS_HEALTH:
                    return  NEWS_HEALTH;
                case URLManager.NEWS_QIWEN:
                    return  NEWS_QIWEN;
                case URLManager.NEWS_MEINV :
                    return  NEWS_MEINV;
                case URLManager.NEWS_VR :
                    return  NEWS_VR;
                case URLManager.NEWS_IT:
                    return  NEWS_IT;
                default:return NEWS_SOCIAL;
            }
        }


    }



}
