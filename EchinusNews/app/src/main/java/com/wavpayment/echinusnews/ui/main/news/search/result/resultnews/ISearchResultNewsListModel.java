package com.wavpayment.echinusnews.ui.main.news.search.result.resultnews;

public interface ISearchResultNewsListModel {
     void requestNewsBySearch(int nowPage, int pageNumber,String searchWord);
}
