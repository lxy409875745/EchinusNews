package com.wavpayment.echinusnews.base;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Administrator on 2018/5/12.
 */

public abstract class BaseFragment extends Fragment {
    private static final String TAG = "BaseFragment";
    protected abstract Object setLayout();
    protected boolean isFristLoad = true;
    protected boolean isUIVisible = false;  //当前是否显示此页

    protected abstract void onCreatView(View rootView, @Nullable Bundle savedInstanceState);
    protected View rootView = null;

    protected <T extends View> T bind(@IdRes int id){
        return rootView.findViewById(id);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Object layout = setLayout();
        if (layout instanceof Integer){
            rootView = inflater.inflate((int)layout,container,false);
        }else if (layout instanceof View){
            rootView = (View) layout;
        }else{
            throw new ClassCastException("layout "+layout+"   not found this layout");
        }
        onCreatView(rootView,savedInstanceState);
        return rootView;
    }

    protected abstract void loadLazyView();
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isUIVisible = isVisibleToUser;
        loadLazyView();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(BaseFragment baseFragment){
        
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }



}
