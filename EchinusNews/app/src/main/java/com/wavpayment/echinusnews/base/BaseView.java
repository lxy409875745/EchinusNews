package com.wavpayment.echinusnews.base;

/**
 * Created by Administrator on 2018/5/12.
 */

public interface BaseView {
    void refresh();
    void refreshDone();
    void refreshError(Exception e);

    void loadMore();
    void loadMoreDone(boolean isNoMoreData);
    void loadError( Exception e);
}
