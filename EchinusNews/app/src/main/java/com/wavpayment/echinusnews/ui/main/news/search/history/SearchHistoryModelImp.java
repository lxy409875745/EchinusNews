package com.wavpayment.echinusnews.ui.main.news.search.history;

import android.content.Context;

import com.wavpayment.echinusnews.comment.db.DBManager;
import com.wavpayment.echinusnews.comment.db.SearchHistoryBeanDao;
import com.wavpayment.echinusnews.comment.network.NetworkHandler;
import com.wavpayment.echinusnews.entity.SearchHistoryBean;

import java.util.List;

public class SearchHistoryModelImp implements ISearchHistoryModel {


    private Context mContext;
    public SearchHistoryModelImp(Context context) {
        mContext = context;
    }


    /**
     * 获取推荐的新闻
     */
    @Override
    public void getRecommendNews(){
        NetworkHandler.getRecommendNews();
    }


    //-------------------------------------------DB------------------------------------------------



    /**
     * 添加历史记录
     *
     * @param historyBean
     */
    @Override
    public void addHistory(SearchHistoryBean historyBean) {
        DBManager.getDaoSession(mContext).getSearchHistoryBeanDao()
                .insert(historyBean);
    }

    /**
     * 删除所有历史记录
     */
    @Override
    public void delAllHistory() {
        DBManager.getDaoSession(mContext).getSearchHistoryBeanDao().deleteAll();
    }

    /**
     * 条件删除历史记录
     *
     * @param searchWord 删除条件
     */
    @Override
    public void delHistory(String searchWord) {
        DBManager.getDaoSession(mContext).getSearchHistoryBeanDao()
                .queryBuilder()
                .where(SearchHistoryBeanDao.Properties.Text.eq(searchWord))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();//条件删除
    }

    /**
     * 获取全部历史记录
     *
     * @return 历史记录
     */
    @Override
    public List<SearchHistoryBean> getHistory() {
        return DBManager.getDaoSession(mContext).getSearchHistoryBeanDao()
                .queryBuilder().list();
    }


    /**
     * 条件查询历史记录
     *
     * @param keywrod
     * @return
     */
    @Override
    public List<SearchHistoryBean> getHistory(String keywrod) {

        return DBManager.getDaoSession(mContext).getSearchHistoryBeanDao()
                .queryBuilder()
                .where(SearchHistoryBeanDao.Properties.Text.eq(keywrod))
                .list();//条件删除
    }
}
