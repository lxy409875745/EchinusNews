package com.wavpayment.echinusnews.utils;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.IdRes;
import android.util.Log;
import android.view.View;

import com.lxy.tools.MainActivity;
import com.wavpayment.echinusnews.R;

import java.security.PublicKey;

public class AnimatorUtils {
    private static final String TAG = "AnimatorUtils";
    public static Animator loadBottomShowAnimator( View view){
        int intw=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
        int inth=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
        view.measure(intw, inth);
        int intwidth = view.getMeasuredWidth();
        int intheight = view.getMeasuredHeight();

        Log.i(TAG, "loadBottomShowAnimator:  view 高度"+view.getMeasuredHeight());
        return ObjectAnimator.ofFloat(view,"translationY", intheight,0);
    }
    public static Animator loadBottomHideAnimator(View view){


        int intw=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
        int inth=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
        view.measure(intw, inth);
        int intwidth = view.getMeasuredWidth();
        int intheight = view.getMeasuredHeight();


        Log.i(TAG, "loadBottomHideAnimator: view 高度"+view.getMeasuredHeight());
        return ObjectAnimator.ofFloat(view,"translationY",0, intheight);
    }
}
