package com.wavpayment.echinusnews.ui.main.news.search.result.resultnews;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.adapter.SearchResultNewsListAdapter;
import com.wavpayment.echinusnews.app.EchinusNewsApplication;
import com.wavpayment.echinusnews.base.BaseFragmentDelegate;
import com.wavpayment.echinusnews.entity.NewsTypeBean;

public class SearchResultNewsListDelegate extends BaseFragmentDelegate implements ISearchResultNewsListView {
    private static final String TAG = "SearchResultNewsListDel";
    private SmartRefreshLayout mSmartRefreshLayout;
    private RecyclerView mRecylerview;
    private SearchResultNewsListPresenterImp mPresenter;
    private TextView mMessageTv;

    private String searchWord;//搜索的关键字
    private NewsTypeBean newsTypeBean;//新闻标题

    private SearchResultNewsListAdapter mAdapter;


    @Override
    public Object setLayout() {
        return R.layout.delegate_search_result_news_list;
    }

    @Override
    public void onBindeView(View rootView, @Nullable Bundle savedInstanceState) {
        initData();
        initViews();
        initPresenter();
        mPresenter.refresh();//直接调用新闻刷新
    }


    private void initPresenter() {
        mPresenter = new SearchResultNewsListPresenterImp(getContext(), mAdapter, this, newsTypeBean);
        mPresenter.setSearchWord(searchWord);
    }


    private void initViews() {
        mMessageTv = bind(R.id.delegate_search_result_news_list_message_tv);

        mRecylerview = bind(R.id.delegate_search_result_news_list_reclyerView);
        mRecylerview.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecylerview.setAdapter(mAdapter = new SearchResultNewsListAdapter(getContext()));
        mAdapter.setOnLoadMoreListener(() -> mPresenter.loadMore()); //上拉加载

        mSmartRefreshLayout = bind(R.id.delegate_search_result_news_list_smartRefreshlayout);
        mSmartRefreshLayout.setOnRefreshListener(refreshLayout -> {
            hideMessage(); //隐藏标题
            mPresenter.refresh(); //下拉刷新
        });
    }

    private void initData() {
        newsTypeBean = getArguments().getParcelable("newsType");
        searchWord = getArguments().getString("searchWord");
    }

    @Override
    public void refreshDone(boolean haveData, int newsCount) {
        Log.i(TAG, "refreshDone: refreshdone      " + haveData);
        hideMessage();
        mSmartRefreshLayout.finishRefresh(true);
        if (haveData) {
            hideMessage();
            if (newsCount < 10) {//如果一次刷新的数据不到10条 则不加载
                mAdapter.loadMoreEnd(); //
            }
        } else {
            showMessage(EchinusNewsApplication.getInstance().getString(R.string.search_result_no_found));
        }
    }

    @Override
    public void loadMoreDone(boolean haveData, int newsCount) {
        Log.i(TAG, "loadMoreDone: loadMoreDone     " + haveData);
        if (haveData) {
            mAdapter.loadMoreComplete();
        } else {
            mAdapter.loadMoreEnd();
        }
    }


    @Override
    public void refreshFail(Exception e) {
        mSmartRefreshLayout.finishRefresh(false);
        if (mAdapter.getData().size() == 0) {
            showMessage(getString(R.string.search_result_no_found));
        } else {
            hideMessage();
        }
    }

    @Override
    public void loadMoreFail(Exception e) {
        mAdapter.loadMoreFail();
    }


    /**
     * 显示提示
     *
     * @param msg
     */
    private void showMessage(String msg) {
        mMessageTv.setText(msg);
        mMessageTv.setVisibility(View.VISIBLE);
    }


    /**
     * 隐藏提示
     */
    private void hideMessage() {
        mMessageTv.setVisibility(View.INVISIBLE);
    }
}
