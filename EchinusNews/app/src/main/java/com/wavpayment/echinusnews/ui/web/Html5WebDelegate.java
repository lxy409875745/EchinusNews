package com.wavpayment.echinusnews.ui.web;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.base.BaseFragmentDelegate;
import com.wavpayment.echinusnews.comment.local.SharePreferenceKey;
import com.wavpayment.echinusnews.entity.NewsBean;
import com.wavpayment.echinusnews.ui.main.news.newslist.NewsListDelegate;
import com.wavpayment.echinusnews.utils.SharePreferenceUtil;
import com.wavpayment.echinusnews.weight.webview.Html5WebView;

import me.yokeyword.fragmentation.anim.DefaultHorizontalAnimator;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

public class Html5WebDelegate extends BaseFragmentDelegate {
    private NewsBean mNewsBean;

    private Html5WebView mWebView;
    private ViewGroup mWebViewContainer;
    private Toolbar mToolbar;
    private ImageView mImageView;
    private View mShareBtnl;//分享
    private CollapsingToolbarLayout mCollapsingToolbarLayout;

    public static Html5WebDelegate creat(NewsBean newsBean) {
        Html5WebDelegate tempDelegate = new Html5WebDelegate();
        tempDelegate.setNewsBean(newsBean);
        return tempDelegate;
    }

    @Override
    public Object setLayout() {
        return R.layout.delegate_html5_web;
    }

    @Override
    public void onBindeView(View rootView, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);//调用menu
        initViews();
        iniData();
    }

    private void iniData() {
        WebSettings settings = mWebView.getSettings();
        int frontSize = (int) SharePreferenceUtil.get(getContext(), SharePreferenceKey.SPKEY_NEWS_DETAIL_FRONT_SIZE, -1);
        switch (frontSize) {
            case -1://无存储
                settings.setTextZoom(100);//默认中号字体
                SharePreferenceUtil.put(getContext(), SharePreferenceKey.SPKEY_NEWS_DETAIL_FRONT_SIZE, 2);//正常字体
                break;
            case 1://小字体
                settings.setTextZoom(70);
                break;
            case 2://中字体
                settings.setTextZoom(100);
                break;
            case 3://大字体
                settings.setTextZoom(150);
                break;
        }
    }


    private void initViews() {
        //imageView
        //設置好Glide選項
        RequestOptions mImageOption = new RequestOptions();
        mImageOption.error(R.drawable.error_img);
        mImageOption.placeholder(R.drawable.loading_img);
        //圖片加載動畫

        DrawableCrossFadeFactory drawableCrossFadeFactory = new DrawableCrossFadeFactory.Builder(200).setCrossFadeEnabled(true).build();
        mImageView = bind(R.id.delegate_html5_web_img);
        Glide.with(getContext())
                .load(mNewsBean.getPicUrl())
                .apply(mImageOption)
                .transition(DrawableTransitionOptions.with(drawableCrossFadeFactory))
                .into(mImageView);

        //webview
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mWebViewContainer = bind(R.id.delegate_html5_web_webviewContainer);
        //防止内存泄露 采用动态添加webview的方式
        mWebView = new Html5WebView(getContext());
        mWebView.setLayoutParams(layoutParams);

        mWebViewContainer.addView(mWebView);
        mWebView.setWebsiteChangeListener(new Html5WebView.WebsiteChangeListener() {
            @Override
            public void onWebsiteChange(String title) {

            }

            @Override
            public void onUrlChange(String url) {

            }
        });

        mWebView.loadUrl(mNewsBean.getUrl());

        //toolbar
        mToolbar = bind(R.id.delegate_html5_web_toolbar);
        mToolbar.setTitle(mNewsBean.getTitle());
        mToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_white_24dp);
        mToolbar.setNavigationOnClickListener(v -> getSupportDelegate().pop());
        mToolbar.inflateMenu(R.menu.menu_html5_web_front_size);
        mToolbar.setOverflowIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_list_white_24dp));
        mToolbar.setOnMenuItemClickListener(item -> { //menu按钮
            WebSettings settings = mWebView.getSettings();
            switch (item.getItemId()) {
                case R.id.menu_html5_web_front_size_refresh: //刷新
                    mWebView.loadUrl(mNewsBean.getUrl());
                    settings.setSupportZoom(true);
                    break;
                case R.id.menu_html5_web_front_size_small://小字体
                    settings.setTextZoom(75);
                    SharePreferenceUtil.put(getContext(), SharePreferenceKey.SPKEY_NEWS_DETAIL_FRONT_SIZE, 1);//保存设置
                    break;
                case R.id.menu_html5_web_front_size_middle://中字体
                    settings.setTextZoom(100);
                    SharePreferenceUtil.put(getContext(), SharePreferenceKey.SPKEY_NEWS_DETAIL_FRONT_SIZE, 2);//保存设置
                    break;
                case R.id.menu_html5_web_front_size_big://大字体
                    settings.setTextZoom(150);
                    SharePreferenceUtil.put(getContext(), SharePreferenceKey.SPKEY_NEWS_DETAIL_FRONT_SIZE, 3);//保存设置
                    break;

            }
            return false;
        });

        //分享floatingActionButton
        mShareBtnl = bind(R.id.delegate_html5_share_btn);
        mShareBtnl.setOnClickListener(v -> { //分享按钮

        });

        //CollapsingToolbarLayout
        mCollapsingToolbarLayout = bind(R.id.delegate_html5_collToolbarLayout);
        mCollapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(getContext(), R.color.color_pureWhite));//设置还没收缩时状态下字体颜色
        mCollapsingToolbarLayout.setCollapsedTitleTextColor(ContextCompat.getColor(getContext(), R.color.color_pureWhite));//设置收缩后Toolbar上字体的颜色
    }

    public void setNewsBean(NewsBean newsBean) {
        mNewsBean = newsBean;
    }


    @Override
    public boolean onBackPressedSupport() {
        return false;
    }

    @Override
    public void onDestroy() {
        if (mWebView != null) {
            mWebView.setVisibility(View.GONE);
            mWebView.removeAllViews();
            mWebView.destroy();
        }
        super.onDestroy();
    }
}
