package com.wavpayment.echinusnews.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.entity.MultiItemEntity;
import com.wavpayment.echinusnews.entity.NewsBean;
import com.wavpayment.echinusnews.entity.SearchHistoryBean;

import java.util.ArrayList;
import java.util.List;

public class SearchNewsAdapter extends BaseMultiItemQuickAdapter<NewsBean,BaseViewHolder> {
    public static final int ITEM_TYPE_NEWS = 0x00000000;//新闻
    public static final int ITEM_TYPE_HISTORY = 0x00000001;//历史记录

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     */
    public SearchNewsAdapter() {
        super(new ArrayList());
        addItemType(ITEM_TYPE_NEWS, R.layout.item_news_normal);
        addItemType(ITEM_TYPE_HISTORY,R.layout.item_search_history);
    }




    private void convertNews(BaseViewHolder helper ,NewsBean item){
        TextView title_tv = helper.getConvertView().findViewById(R.id.item_news_tv_title);
        TextView config_tv = helper.getConvertView().findViewById(R.id.item_news_tv_config); //说明和日周期
        ImageView picture_img = helper.getConvertView().findViewById(R.id.item_news_img);
        title_tv.setText(item.getTitle());
        config_tv.setText(item.getDescription()+"   "+item.getCtime());
        Glide.with(mContext).load(item.getPicUrl()).into(picture_img);

    }




    @Override
    protected void convert(BaseViewHolder helper, NewsBean item) {
        convertNews(helper,item);
    }
}
