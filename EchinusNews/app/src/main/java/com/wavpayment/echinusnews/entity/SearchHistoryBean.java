package com.wavpayment.echinusnews.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.Transient;

@Keep
@Entity
public class SearchHistoryBean  {
    @Id(autoincrement = true)
    Long id;


    String text;



    public static SearchHistoryBean creatHistoryBean(String text){
        return new SearchHistoryBean(text);
    }

    public SearchHistoryBean(String text) {
        this.text = text;
    }

    public SearchHistoryBean(Long id, String text) {
        this.id = id;
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
