package com.wavpayment.echinusnews.ui.main.news.search.result.resultnews;

public interface ISearchResultNewsListView {
    void refreshDone(boolean haveData,int newsCount);

    void loadMoreDone(boolean haveData,int newsCount);

    void refreshFail(Exception e);

    void loadMoreFail(Exception e);

}
