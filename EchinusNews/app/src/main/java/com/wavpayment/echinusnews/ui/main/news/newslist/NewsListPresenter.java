package com.wavpayment.echinusnews.ui.main.news.newslist;

import android.content.Context;
import android.util.Log;
import com.lxy.spone.config.ConfigKey;
import com.lxy.spone.config.Spone;

import com.wavpayment.echinusnews.adapter.NewsListViewAdapter;
import com.wavpayment.echinusnews.base.BasePresenter;
import com.wavpayment.echinusnews.base.BaseView;
import com.wavpayment.echinusnews.entity.NewsBean;
import com.wavpayment.echinusnews.entity.NewsResultBean;
import com.wavpayment.echinusnews.entity.NewsTypeBean;
import com.wavpayment.echinusnews.utils.NetUtils;

import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;

/**
 * Created by lenovo on 2018/5/21.
 */

public class NewsListPresenter extends BasePresenter<BaseView> {
    private static final String TAG = "NewsListPresenter";
    private static final int MAX_PAGE = 20;//最大頁數
    private int nowPage = 1;//当前页数
    private static final int PAGE_NUMBER = 10;//每一页条数
    private boolean isRefreshing = false;

    private NewsListViewAdapter mAdapter;
    private NewsTypeBean mNewsTypeBean;
    private NewslIstModle mModle;

    public NewsListPresenter(Context context, BaseView view, NewsListViewAdapter adapter, NewsTypeBean newsTypeBean) {
        super(context,view);
        this.mAdapter = adapter;
        this.mNewsTypeBean = newsTypeBean;
        initModle();
    }

    private boolean isLoadDataFromLocal = false; //判斷數據是否從本地獲取

    private void initModle() {
        mModle = new NewslIstModle(mContext, mNewsTypeBean) {
            @Override
            public void netRequestCallback(Object value) { //从网络请求获取数据
                NewsResultBean resultBean = (NewsResultBean) value;
                if (resultBean != null && resultBean.getNewslist().size() > 0) { //返回成功
                    if (isRefreshing) {
                        NewsBean requestNewsBean = resultBean.getNewslist().get(0);
                        List<NewsBean> localNewsBeans = mModle.queryNewsBeanByRequestTime(requestNewsBean.getNewsType());
                        if (localNewsBeans != null && localNewsBeans.size() > 0) {//當前本地有該類型數據
                            NewsBean localNewsBean = localNewsBeans.get(0);
                            if (localNewsBean.getTitle().equals(requestNewsBean.getTitle())) {//當本地數據中最旧的一條數據比對更新下來的數據相等時 判定當前網絡數據沒有更新
                                // List<NewsBean> localData = mModle.queryNewsBeanByNewsType(requestNewsBean.getNewsType(), nowPage, PAGE_NUMBER); 不需要多余查询
                                mAdapter.setNewData( resultBean.getNewslist());
                                isLoadDataFromLocal = true;
                            } else {
                                mModle.deleteByNewsType(requestNewsBean.getNewsType());//清除該分類的所有本地新聞數據
                                addDataFromRequest(isRefreshing, resultBean.getNewslist()); //添加新的新闻数据
                            }
                        } else {
                            addDataFromRequest(isRefreshing, resultBean.getNewslist());
                        }
                        refreshDone();
                    } else {
                        addDataFromRequest(isRefreshing, resultBean.getNewslist());
                        loadMoreDone();
                    }
                    nowPage++;
                } else {
                    if (isRefreshing) {
                        refreshError(new Exception("刷新失败"));
                    } else {
                        loadError(new Exception("加载失败"));
                    }
                }
                resetRefreshFlag();
            }


            @Override
            public void localRequestCallback(Object value) {   //从本地获取数据
                List<NewsBean> localNewsBeans = (List<NewsBean>) value;
                if (isRefreshing){
                    if (localNewsBeans != null && localNewsBeans.size() > 0) { //如果当前有本地目录
                        isLoadDataFromLocal = true;
                        mAdapter.setNewData(localNewsBeans);
                        nowPage++;
                        resetRefreshFlag();
                        refreshDone();
                    }else { //如果当前没有该数据 则尝试从网络获取
                        mModle.netRequestData(nowPage,PAGE_NUMBER);

                    }
                }else {
                    if (localNewsBeans != null && localNewsBeans.size() > 0) {
                        mAdapter.addData(localNewsBeans);
                        nowPage++;
                        loadMoreDone();
                    }else {
                        mModle.netRequestData(nowPage,PAGE_NUMBER);
                    }
                }
            }
        };
    }

    //從網絡添加數據到適配器和本地
    private void addDataFromRequest(boolean isRefreshing, List<NewsBean> netRequestData) {
        isLoadDataFromLocal = false;
        for (NewsBean tempBean : netRequestData) {
            Long nowTime = System.currentTimeMillis();
            Date tempTime = new Date(nowTime);
            tempBean.setRequestTime(tempTime);
        }
        if (isRefreshing) {
            mAdapter.setNewData(netRequestData);
        } else {
            mAdapter.addData(netRequestData);
        }
        mModle.insertData(netRequestData);
    }


    private void resetRefreshFlag() {
        if (isRefreshing) {
            isRefreshing = false;
        }
    }


    @Override
    protected void refreshDone() {
        mView.get().refreshDone();
    }


    @Override
    protected void loadMoreDone() {
        if (nowPage >= MAX_PAGE) {
            mView.get().loadMoreDone(true);
        } else {
            mView.get().loadMoreDone(false);
        }
    }

    /**
     * 刷新
     */
    @Override
    public void refresh() {
        isRefreshing = true;
        nowPage = 1;
        int net_status = Spone.getConfiguration(ConfigKey.NET_STATUS);
        if (NetUtils.isNetworkConnected(mContext) ) {
            isLoadDataFromLocal = false;
            mModle.netRequestData( nowPage,PAGE_NUMBER);
        } else {//无网络 //尝试加载本地数据
            mModle.localRequestData( nowPage, PAGE_NUMBER);

        }
    }

    /**
     * 加载更多
     */
    @Override
    public void loadMore() {
        if (!isRefreshing) {
            if (isLoadDataFromLocal) {//当前数据从本地加载
                if (nowPage >= MAX_PAGE) {
                    loadMoreDone();
                }else {
                    mModle.localRequestData( nowPage, PAGE_NUMBER);
                }
            } else {
                if (nowPage >= MAX_PAGE) {
                    loadMoreDone();
                }else {
                    if (NetUtils.isNetworkConnected(Spone.getApplicationContext())){ //判断当前是否有网络
                        mModle.netRequestData( nowPage,PAGE_NUMBER);
                    }else {
                        mModle.localRequestData(nowPage,PAGE_NUMBER);
                    }
                }

            }
        }
    }

    /** 加载失败
     * @param e
     */
    @Override
    protected void loadError(Exception e) {
        mView.get().loadError(e);
    }
    /** 刷新失败
     * @param e
     */
    protected void refreshError(Exception e) {
        mView.get().refreshError(e);
    }
}
