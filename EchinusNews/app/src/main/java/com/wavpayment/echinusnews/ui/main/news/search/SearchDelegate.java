package com.wavpayment.echinusnews.ui.main.news.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.base.BaseFragmentDelegate;
import com.wavpayment.echinusnews.ui.main.news.search.history.SearchHistoryDelegate;
import com.wavpayment.echinusnews.ui.main.news.search.result.SearchResultDelegate;
import com.wavpayment.echinusnews.utils.ToastUtils;

import java.util.ArrayList;

import me.yokeyword.fragmentation.ISupportFragment;


/**
 *
 */
public class SearchDelegate extends BaseFragmentDelegate {
    private ViewGroup mPageContainer;//切换页面的容器
    private EditText mEditText;//输入框
    private View mSearchBtn;//搜索按钮
    private View mDeleteTextBtn;//删除搜索文字按钮

    private ArrayList<BaseFragmentDelegate> DELEGATES = new ArrayList<>();

    @Override
    public Object setLayout() {
        return R.layout.delegate_search;
    }

    @Override
    public void onBindeView(View rootView, @Nullable Bundle savedInstanceState) {

    }


    private void initViews() {
        mPageContainer = bind(R.id.delegate_search_page_container);
        mEditText = bind(R.id.delegate_search_edt);
        mEditText.setOnEditorActionListener((v, actionId, event) -> { //设置监听搜索
            if (actionId == EditorInfo.IME_ACTION_SEND || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) { //设置软键盘搜索监听
                showResult(mEditText.getText().toString());
                return true;
            }
            return false;
        });


        mSearchBtn = bind(R.id.delegate_search_btn);
        mDeleteTextBtn = bind(R.id.delegate_search_clear_btn);
        initPageContainer();
        initSearchBtn();
        initEdittext();
        initDeleteBtn();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private BaseFragmentDelegate nowPage;//当前页面

    private void initPageContainer() {
        DELEGATES.add(nowPage = new SearchHistoryDelegate());  //加载历史页面
        DELEGATES.add(new SearchResultDelegate()); //搜索結果頁面
        SearchHistoryDelegate historyDelegate = (SearchHistoryDelegate) nowPage;
        historyDelegate.setHistoryItemClickCallback(searchWrodHistory -> {
            mEditText.setText(searchWrodHistory);//设置文字到edt上
            mEditText.setSelection(searchWrodHistory.length());//将光标移至文字末尾
            showResult(searchWrodHistory); //设置点击历史记录的按钮监听
        });
        getSupportDelegate().loadMultipleRootFragment(R.id.delegate_search_page_container, 0, DELEGATES.toArray(new ISupportFragment[2]));//先显示历史页面
        getSupportDelegate().showHideFragment(DELEGATES.get(0), DELEGATES.get(1));
    }

    /**
     * 删除按钮
     */
    private void initDeleteBtn() {
        mDeleteTextBtn.setOnClickListener(v -> {
            showHistory();
            mEditText.setText("");
        });
    }
    
    private void initEdittext() {
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    mDeleteTextBtn.setVisibility(View.INVISIBLE);
                } else {
                    mDeleteTextBtn.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /**
     * 搜索按钮
     */
    private void initSearchBtn() {
        mSearchBtn.setOnClickListener(v -> {  //搜索
            showResult(mEditText.getText().toString());
        });
    }


    /**
     * 显示历史记录
     */
    private void showHistory() {
        getSupportDelegate().showHideFragment(DELEGATES.get(0), nowPage);
        nowPage = DELEGATES.get(0);
        SearchHistoryDelegate historyDelegate = (SearchHistoryDelegate) nowPage;
        historyDelegate.showHistory();
    }


    /**
     * 显示搜索结果页面
     *
     * @param searchWord
     */
    private void showResult(String searchWord) {
        hideKeyboard(mEditText);
        if (searchWord.isEmpty()) {
            ToastUtils.warning(getString(R.string.search_wraning_empty));
            return;
        }

        saveSearchWord(searchWord);  //先保存到历史记录
        getSupportDelegate().showHideFragment(DELEGATES.get(1), nowPage);
        nowPage = DELEGATES.get(1);

        SearchResultDelegate resultDelegate = (SearchResultDelegate) nowPage;
        resultDelegate.search(searchWord);
    }


    /**
     * 存入到历史记录中
     *
     * @param searchWrod
     */
    private void saveSearchWord(String searchWrod) {
        SearchHistoryDelegate historyDelegate = (SearchHistoryDelegate) DELEGATES.get(0);
        historyDelegate.addSearchWord(searchWrod);//每次搜索需要先保存
    }


    @Override
    public boolean onBackPressedSupport() {
        if (nowPage instanceof SearchHistoryDelegate) { //如果是历史记录界面则直接退出
            return false;//直接退出
        } else { //是搜索结果界面则先返回到历史记录界面再退出
            showHistory();
            return true;
        }
    }



}
