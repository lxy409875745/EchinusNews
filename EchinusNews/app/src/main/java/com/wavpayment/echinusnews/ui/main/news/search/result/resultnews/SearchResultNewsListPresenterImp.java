package com.wavpayment.echinusnews.ui.main.news.search.result.resultnews;

import android.content.Context;
import android.util.Log;

import com.wavpayment.echinusnews.adapter.SearchResultNewsListAdapter;
import com.wavpayment.echinusnews.comment.callback.CallbackConfig;
import com.wavpayment.echinusnews.comment.callback.CallbackManager;
import com.wavpayment.echinusnews.comment.network.URLManager;
import com.wavpayment.echinusnews.entity.NewsBean;
import com.wavpayment.echinusnews.entity.NewsResultBean;
import com.wavpayment.echinusnews.entity.NewsTypeBean;
import java.lang.ref.WeakReference;
import java.util.List;

public class SearchResultNewsListPresenterImp implements ISearchResultNewsListPresenter {
    private static final String TAG = "SearchResultNewsListPre";
    private Context mContext;
    private SearchResultNewsListAdapter mAdapter;
    private WeakReference<ISearchResultNewsListView> mView;
    private ISearchResultNewsListModel mModel;
    private NewsTypeBean mNewsTypeBean;
    private String searchWord;

    private int nowPage = 1;//当前第几页
    private static final int PAGE_NUM = 20;//每一页的数量
    private boolean isRefreshing = false;


    public SearchResultNewsListPresenterImp(Context context, SearchResultNewsListAdapter adapter, ISearchResultNewsListView view, NewsTypeBean newsTypeBean) {
        this.mContext = context;
        this.mAdapter = adapter;
        this.mView = new WeakReference(view);
        this.mNewsTypeBean = newsTypeBean;

        this.mModel = new SearchResultNewsListModelImp(mContext, mNewsTypeBean);
        initCallback();

    }

    private void initCallback() {
        CallbackManager.getInstance().addCallback( URLManager.Type.containsUrl2Type(mNewsTypeBean.getUrl()), value -> {  //新闻数据
            NewsResultBean resultBean = (NewsResultBean) value;
            Log.i(TAG, "initCallback: resultBean = "+resultBean);
            if (resultBean != null ){
                List<NewsBean> newsBeans = resultBean.getNewslist();  //获取新闻数据
                if (isRefreshing){ //当前是刷新数据
                    if (newsBeans == null || newsBeans.size() == 0){
                        mView.get().refreshDone(false,0);
                    }else {
                        mAdapter.setNewData(newsBeans);
                        mView.get().refreshDone(true,newsBeans.size());
                    }
                }else {
                    if (newsBeans == null || newsBeans.size() == 0){
                        mView.get().loadMoreDone(false,0);
                    }else {
                        mAdapter.addData(newsBeans);
                        mView.get().loadMoreDone(true,newsBeans.size());
                    }
                }

                nowPage ++;
            }else {
                if (isRefreshing) {
                    refreshError(new Exception("刷新失败"));
                } else {
                    loadMoreError(new Exception("加载失败"));
                }
            }
            isRefreshing = false;
        });
    }

    @Override
    public void setSearchWord(String searchWord) {
        this.searchWord = searchWord;
    }

    @Override
    public void refresh() {
        isRefreshing = true;
        nowPage = 0;
        mModel.requestNewsBySearch(nowPage,PAGE_NUM,searchWord);
    }

    @Override
    public void loadMore() {
        if (!isRefreshing){
            mModel.requestNewsBySearch(nowPage,PAGE_NUM,searchWord);
        }
    }

    /**
     * 清除所有新闻
     */
    @Override
    public void clearNews() {
        mAdapter.setNewData(null);
    }

    private void loadMoreError(Exception e) {
        mView.get().loadMoreFail(e);
    }

    private void refreshError(Exception e) {
        mView.get().refreshFail(e);
    }

}
