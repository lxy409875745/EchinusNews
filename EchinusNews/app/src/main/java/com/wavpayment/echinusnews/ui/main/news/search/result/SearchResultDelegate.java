package com.wavpayment.echinusnews.ui.main.news.search.result;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.flyco.tablayout.SlidingTabLayout;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.adapter.SearchHistoryResultPagerAdapter;
import com.wavpayment.echinusnews.base.BaseFragmentDelegate;


public class SearchResultDelegate extends BaseFragmentDelegate {

    private SlidingTabLayout mTablayout;
    private ViewPager mViewpager;//

    private SearchHistoryResultPagerAdapter mAdapter;

    @Override
    public Object setLayout() {
        return R.layout.delegate_search_result;
    }

    @Override
    public void onBindeView(View rootView, @Nullable Bundle savedInstanceState) {
        initViewpager();
        initTabLayout();
    }

    private void initViewpager() {
        mViewpager = bind(R.id.delegate_search_result_viewpager);
        mAdapter = new SearchHistoryResultPagerAdapter(getContext(),getChildFragmentManager());
        mViewpager.setAdapter(mAdapter);
    }

    private void initTabLayout() {
        mTablayout = bind(R.id.delegate_search_result_tabLayout);
        mAdapter.addTabLayout(mTablayout);
        mTablayout.setViewPager(mViewpager);
    }



    public void search(String searchWrod){//搜索
        mAdapter.upDataSearchWordData(searchWrod);
    }


}
