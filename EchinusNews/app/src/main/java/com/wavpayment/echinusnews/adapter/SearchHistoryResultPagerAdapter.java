package com.wavpayment.echinusnews.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.flyco.tablayout.SlidingTabLayout;
import com.wavpayment.echinusnews.entity.NewsTypeBean;
import com.wavpayment.echinusnews.ui.main.news.NewsModel;
import com.wavpayment.echinusnews.ui.main.news.search.result.resultnews.SearchResultNewsListDelegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchHistoryResultPagerAdapter extends FragmentStatePagerAdapter {
    private List<NewsTypeBean> titles = new ArrayList<>();
    private HashMap<String, Fragment> fragmentMap = new HashMap<>();
    private SlidingTabLayout mTabLayout;
    private Context mContext;
    private NewsModel mNewsModel;
    private FragmentManager fm;

    public SearchHistoryResultPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.fm = fm;
        mContext = context;
        mNewsModel = new NewsModel(context);
        initNewsTypeData(mNewsModel.getNewsTypes());
    }

    /**
     * 添加数据
     *
     * @param data
     */
    public void initNewsTypeData(List<NewsTypeBean> data) {
        titles.addAll(data);

    }

    public void addTabLayout(SlidingTabLayout tablayout) {
        mTabLayout = tablayout;
    }


    @Override
    public Fragment getItem(int position) {
        Fragment tempFragment = fragmentMap.get(titles.get(position));
        if (tempFragment == null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("newsType", titles.get(position));
            bundle.putString("searchWord", mSearchWord);
            tempFragment = new SearchResultNewsListDelegate();
            tempFragment.setArguments(bundle);
            fragmentMap.put(titles.get(position).getTitle(), tempFragment);
        }
        return tempFragment;
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position).getTitle();
    }


    private String mSearchWord;

    public void upDataSearchWordData(String searchWord) {
        if (mSearchWord != null && !mSearchWord.equals(searchWord)) {
            fragmentMap.clear();

        }
        this.mSearchWord = searchWord;

        notifyDataSetChanged();


    }

    @Override
    public int getItemPosition(Object object) {
        // TODO Auto-generated method stub
        return PagerAdapter.POSITION_NONE;
    }


}
