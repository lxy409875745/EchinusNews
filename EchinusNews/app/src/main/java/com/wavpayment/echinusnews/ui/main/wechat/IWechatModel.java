package com.wavpayment.echinusnews.ui.main.wechat;

import com.wavpayment.echinusnews.comment.db.DBManager;
import com.wavpayment.echinusnews.comment.db.NewsBeanDao;
import com.wavpayment.echinusnews.entity.NewsBean;

import java.util.List;

interface IWechatModel {



    void requestDataFromNet(int nowPage, int pageNumber);

    void requestDataFromLocal(int nowPage, int pageNumber);

    //---------------------------------------------------db
    //插入一條數據
    void insertData(NewsBean newsBean);

    //插入一套數據
    void insertData(List<NewsBean> newsBeans);

    void deleteAll();

    //刪除某一分類的新聞
    void deleteByNewsType(String newsType);

    //獲取某個分類中 最開始的那一條數據
    List<NewsBean> queryNewsBeanByRequestTime(String newsType);

    //獲取分類數據
    List<NewsBean> queryNewsBeanByNewsType(String newsType, int nowPage, int pageNumber);

    //獲取某個分類的數量
    long queryNewsBeanNewsTypeCount(String newsType);
}
