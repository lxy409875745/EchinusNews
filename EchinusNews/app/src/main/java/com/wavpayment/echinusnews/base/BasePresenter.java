package com.wavpayment.echinusnews.base;

import android.content.Context;

import com.wavpayment.echinusnews.adapter.NewsListViewAdapter;
import com.wavpayment.echinusnews.entity.NewsTypeBean;

import java.lang.ref.WeakReference;

public abstract class BasePresenter<T extends BaseView> {
    protected Context mContext;
    protected WeakReference<T> mView;

    public BasePresenter(Context context, T view) {
        mContext = context;
        mView = new WeakReference<>(view);
    }



   protected abstract void refresh();

    protected abstract void refreshDone();

    protected  abstract void refreshError(Exception e);

    protected  abstract void loadMore();

    protected abstract void loadMoreDone();

   protected abstract void loadError(Exception e);


}
