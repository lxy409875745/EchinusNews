package com.wavpayment.echinusnews.app;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.lxy.spone.config.ConfigKey;
import com.lxy.spone.config.Spone;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.base.BaseActivityDelegate;
import com.wavpayment.echinusnews.base.BaseFragmentDelegate;
import com.wavpayment.echinusnews.comment.callback.EventBusPosKey;
import com.wavpayment.echinusnews.ui.luncher.LuncherDelegate;
import com.wavpayment.echinusnews.utils.DialogManager;
import com.wavpayment.echinusnews.utils.ResourceUtils;
import com.wavpayment.echinusnews.utils.StatusBarCompat;
import com.wavpayment.echinusnews.utils.StatusBarUtil;
import com.wavpayment.echinusnews.utils.ToastUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import me.yokeyword.fragmentation.anim.DefaultHorizontalAnimator;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

public class EchinusActivity extends BaseActivityDelegate {
    private static final String TAG = "EchinusActivity";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DialogManager.getInstance().init(this);
      //  StatusBarCompat.translucentStatusBar(this);

    }

    @Override
    public BaseFragmentDelegate setLayout() {
        return new LuncherDelegate();
    }


    //修改状态栏颜色
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBarColorStatus(EventBusPosKey key) {

   /*     if (key == EventBusPosKey.CHANGE_BLUE_STATE_BAR){//蓝色底部
            StatusBarCompat.setStatusBarColor(EchinusActivity.this, ResourceUtils.getColor(EchinusActivity.this,R.color.app_color));
        }
        if (key ==EventBusPosKey.CHANGE_GREEN_STATE_BAR){//绿色底部
            StatusBarCompat.setStatusBarColor(EchinusActivity.this, ResourceUtils.getColor(EchinusActivity.this,R.color.color_green_500));
        }
        if (key == EventBusPosKey.CHANGE_ORANGE_STATE_BAR){//橘色底部
            StatusBarCompat.setStatusBarColor(EchinusActivity.this, ResourceUtils.getColor(EchinusActivity.this,R.color.color_orange_500));
        }*/

        if (key == EventBusPosKey.CHANGE_BLUE_STATE_BAR) {//蓝色底部
            StatusBarCompat.setStatusBarColor(EchinusActivity.this, ResourceUtils.getColor(EchinusActivity.this, R.color.app_color));
        }
        if (key == EventBusPosKey.CHANGE_GREEN_STATE_BAR) {//绿色底部
            StatusBarCompat.setStatusBarColor(EchinusActivity.this, ResourceUtils.getColor(EchinusActivity.this, R.color.app_color));
        }
        if (key == EventBusPosKey.CHANGE_ORANGE_STATE_BAR) {//橘色底部
            StatusBarCompat.setStatusBarColor(EchinusActivity.this, ResourceUtils.getColor(EchinusActivity.this, R.color.app_color));
        }


    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    private void onNetStatusChange(EventBusPosKey key) {
        if (key == EventBusPosKey.NET_STATUS_CHANGE) {
            int net_status = Spone.getConfiguration(ConfigKey.NET_STATUS);//获取当前网络状态
            switch (net_status) {
                case 0: //wifi有网
                    break;
                case 1://移动数据有网
                    //有网络操作
                    break;
                case 2://无网络
                    ToastUtils.warning(getString(R.string.comment_wraning_net_disconnect));
                    break;
            }
        }
    }



}
