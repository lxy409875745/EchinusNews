package com.wavpayment.echinusnews.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.entity.NewsBean;

import java.util.ArrayList;
import java.util.List;

public class SearchHistoryGridViewAdapter extends BaseAdapter{
    private List<NewsBean> data = new ArrayList<>();
    private Context mContext;


    public SearchHistoryGridViewAdapter(Context context) {
        mContext = context;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_news_grid,parent,false);
        TextView title_tv = itemView.findViewById(R.id.item_news_grid_title_tv);  //标题
        TextView description_tv = itemView.findViewById(R.id.item_news_grid_description_tv);//出处
        TextView time_tv = itemView.findViewById(R.id.item_news_grid_time_tv); //时间
        ImageView img = itemView.findViewById(R.id.item_news_grid_img);//图片

        NewsBean itemData = data.get(position);

        title_tv.setText(itemData.getTitle());
        description_tv.setText(itemData.getDescription());
        time_tv.setText(itemData.getCtime());
        Glide.with(mContext).load(itemData.getPicUrl()).into(img);

        return itemView;
    }

    public void setData(NewsBean data){
        if (data != null){
            this.data.add(data);
        }
        notifyDataSetChanged();
    }

    /** 添加数据
     * @param data
     */
    public void setData(List<NewsBean> data){
        if (data != null){
            this.data.addAll(data);
        }
        notifyDataSetChanged();
    }


    /** 设置新数据
     * @param data
     */
    public void setNewsData(List<NewsBean> data){
        this.data.clear();
        if (data != null){
           this.data.addAll(data);
        }
        notifyDataSetChanged();
    }



    public void setNewsData(NewsBean data){
        this.data.clear();
        if (data != null){
            this.data.add(data);
        }
        notifyDataSetChanged();
    }


    /**
     * 清除所有
     */
    public void removeAll(){
        this.data.clear();
        notifyDataSetChanged();
    }


}
