package com.wavpayment.echinusnews.other;

import com.google.gson.Gson;
import com.wavpayment.echinusnews.entity.NewsResultBean;

/**
 * Created by Administrator on 2018/5/12.
 */

public class JsonReader {
    private static Gson gson = null;
    private JsonReader() {
        gson = new Gson();
    }

    private static class Handler {
        private static final JsonReader INSTANSE = new JsonReader();
    }

    public static JsonReader getInstanse (){
        return Handler.INSTANSE;
    }


    public NewsResultBean readNews(String json){
        NewsResultBean resultBean  = gson.fromJson(json,NewsResultBean.class);
        return resultBean;
    }
}
