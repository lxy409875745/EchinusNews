package com.wavpayment.echinusnews.ui.main.wechat;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.vondear.rxtool.view.RxToast;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.adapter.WechatListAdapter;
import com.wavpayment.echinusnews.base.BaseFragmentEventBusDelegate;
import com.wavpayment.echinusnews.comment.callback.EventBusPosKey;
import com.wavpayment.echinusnews.utils.NetUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Administrator on 2018/5/11.
 */

public class WechatDelegate extends BaseFragmentEventBusDelegate implements IWechatView {
    private static final String TAG = "WechatDelegate";
    private RecyclerView mRecyclerview;
    private SmartRefreshLayout mSmartRefreshLayout;
    private WechatListAdapter mAdapter;
    private WechatPresenter mPresenter;
    private GridLayoutManager layoutManager;
    private Handler mHandler = new Handler();

    private boolean isVisible = false; //当前页面是否是显示的状态

    @Override
    public Object setLayout() {
        return R.layout.delegate_wechat;
    }

    @Override
    public void onBindeView(View rootView, @Nullable Bundle savedInstanceState) {
        initViews();
        initPresenter();
        initData();

    }

    //-----------------------------------------------------------------------------------------------presenter
    private void initPresenter() {
        mPresenter = new WechatPresenter(getContext(), mAdapter, mHandler, this);
    }

    //------------------------------------------------------------------------------------------------处理view

    private void initViews() {
        initRefreshView();
        initRecyclerview();
    }

    /**
     * 刷新控件
     */
    private void initRefreshView() {
        mSmartRefreshLayout = bind(R.id.delegate_wechat_smartRefresh);
        mSmartRefreshLayout.setOnRefreshListener(refreshLayout -> mPresenter.refresh());
    }

    /**
     * recyclerview
     */
    private void initRecyclerview() {
        mRecyclerview = bind(R.id.delegate_wechat_recyclerview);
        mRecyclerview.setAdapter(mAdapter = new WechatListAdapter(getContext()));
        mAdapter.setOnLoadMoreListener(() -> mPresenter.loadMore());
        View emptyView = LayoutInflater.from(getContext()).inflate(R.layout.layout_empty_view, null, false);
        emptyView.setOnClickListener(v -> { //空数据

        });
        mAdapter.setEmptyView(emptyView);
        layoutManager = new GridLayoutManager(getContext(), 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() { //设置layoutmanager数量
            @Override
            public int getSpanSize(int position) {

                int lenth = String.valueOf(position).length();
                String lastchar = String.valueOf(position).substring(lenth - 1, lenth);
                int lastNumber = Integer.valueOf(lastchar);//最后一位
                if (position == 0 || position % 10 == 0 || lastNumber % 7 == 0) {
                    return 2;
                } else {
                    return 1;
                }
            }
        });


        //dy > 0 时为手指向上滚动,列表滚动显示下面的内容
        //dy < 0 时为手指向下滚动,列表滚动显示上面的内容
        mRecyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {//向上滑動 隱藏菜單  判断底部菜单是否显示
                    //Log.i(TAG,"向上滑动");
                    EventBus.getDefault().post(EventBusPosKey.HIDE_BOTTOM_NAVIGATIONBAR);
                    EventBus.getDefault().post(EventBusPosKey.SHOW_BOTTOM_TO_TOP_BOTTON);
                } else if (dy < -40) {//向下滑動顯示菜單
                    // Log.i(TAG,"向下滑动");
                    EventBus.getDefault().post(EventBusPosKey.SHOW_BOTTOM_NAVIGATIONBAR);
                    EventBus.getDefault().post(EventBusPosKey.HIDE_BOTTOM_TO_TOP_BOTTON);

                }
            }
        });


        mRecyclerview.setLayoutManager(layoutManager);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void scorllTopEvent(EventBusPosKey key) {
        if (key == EventBusPosKey.SCROLL_TO_TOP) {
            if (mRecyclerview != null && isVisible) {
                mRecyclerview.scrollToPosition(0);
            }
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            isVisible = false;
        } else {
            isVisible = true;
        }
    }


    //------------------------------------------------------------------------------------------------初始化数据
    private void initData() { //初始化数据
        mPresenter.refresh();
    }

    //-------------------------------------------------------------------------------------------------处理上下拉加载刷新
    @Override
    public void refreshDone(boolean haveData, int count) {
        mHandler.post(() -> {
            if (NetUtils.isNetworkConnected(getContext())) {
                mSmartRefreshLayout.finishRefresh(true);
            } else {
                RxToast.error(getString(R.string.comment_wraning_net_disconnect));
                mSmartRefreshLayout.finishRefresh(false);//没网的时候还是刷新失败
            }
        });

    }

    @Override
    public void loadmoreDone(boolean haveData, int cound) {
        mHandler.post(() -> {
            if (haveData) {
                mAdapter.loadMoreComplete();
            } else {
                mAdapter.loadMoreEnd();
            }
        });

    }

    @Override
    public void refreshFail(Exception e) {
        mSmartRefreshLayout.finishRefresh(false);
        if (NetUtils.isNetworkConnected(getContext())) {
            RxToast.error(getString(R.string.comment_wraning_net_disconnect));
        }
    }

    @Override
    public void loadmoreFail(Exception e) {
        mAdapter.loadMoreFail();
    }


}
