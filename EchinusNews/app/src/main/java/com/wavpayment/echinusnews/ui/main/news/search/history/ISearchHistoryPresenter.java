package com.wavpayment.echinusnews.ui.main.news.search.history;

import com.wavpayment.echinusnews.entity.SearchHistoryBean;


public interface ISearchHistoryPresenter {

    /** 添加历史记录
     * @param searchWord
     */
    void addHistory(String searchWord);

    /**
     * 删除所有历史记录
     */
    void delAllHistory();

    /**
     * 删除历史记录
     */
    void delHistory(String searchWord);

    /**
     * 获取history
     */
    void showHistory();


    /**
     * 显示推荐的新闻
     */
    void showRecommendNews();



}
