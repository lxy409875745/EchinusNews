package com.wavpayment.echinusnews.comment.callback;

import java.util.WeakHashMap;

/**
 * Created by Administrator on 2018/5/11.
 */

public class CallbackManager {
    private static final WeakHashMap<Object,ICallback> callbackMap = new WeakHashMap<>();

    private static class Handler{
        private static final CallbackManager INSTANSE = new CallbackManager();
    }

    public static CallbackManager getInstance(){
        return Handler.INSTANSE;
    }

    public void addCallback(Object key,ICallback callback){
        callbackMap.put(key,callback);
    }
    public ICallback getCallback (Object key){
        return callbackMap.get(key);
    }
}
