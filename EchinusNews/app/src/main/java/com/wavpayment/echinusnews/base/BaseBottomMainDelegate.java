package com.wavpayment.echinusnews.base;

import android.animation.Animator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.comment.callback.EventBusPosKey;
import com.wavpayment.echinusnews.ui.main.news.NewsDelegate;
import com.wavpayment.echinusnews.ui.main.tools.ToolsDelegate;
import com.wavpayment.echinusnews.ui.main.wechat.WechatDelegate;
import com.wavpayment.echinusnews.utils.AnimatorUtils;
import com.wavpayment.echinusnews.utils.ResourceUtils;
import com.wavpayment.echinusnews.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import me.yokeyword.fragmentation.ISupportFragment;

/**
 * Created by Administrator on 2018/5/11.
 */

public abstract class BaseBottomMainDelegate extends BaseQuitDoubleClickDelegate implements BottomNavigationBar.OnTabSelectedListener {
    private static final String TAG = "BaseBottomMainDelegate";
    private BottomNavigationBar navigationBar;

    private ArrayList<BaseFragmentDelegate> DELEGATES = new ArrayList<>();

    private BaseFragmentDelegate nowDelege;

    private boolean isLoadingAnim = false;
    private boolean barIsShowing = true;

    private FloatingActionButton mTopFabtn; //返回最上层

    private int nowPosition = 0;//当前位置

    @Override
    public Object setLayout() {
        return R.layout.delegate_main;
    }

    @Override
    public void onBindeView(View rootView, @Nullable Bundle savedInstanceState) {
        initDelegates();
        initNavigationBar();
        initTopFabBtn();
        initAnimator();
    }

    private void initDelegates() {
        DELEGATES.add(nowDelege = new NewsDelegate());
        DELEGATES.add(new WechatDelegate());
        DELEGATES.add(new ToolsDelegate());
        getSupportDelegate().loadMultipleRootFragment(R.id.delegate_main_delegateContainer, 0, DELEGATES.toArray(new ISupportFragment[3]));
    }

    private void initNavigationBar() {
        EventBus.getDefault().post(EventBusPosKey.CHANGE_BLUE_STATE_BAR);
        navigationBar = bind(R.id.delegate_main_bottomnavbar);
       /* BottomNavigationItem item_1 = new BottomNavigationItem(R.drawable.fragmentation_help, "navBtn01").setActiveColor(ResourceUtils.getColor(getContext(), R.color.app_color));
        BottomNavigationItem item_2 = new BottomNavigationItem(R.drawable.fragmentation_ic_right, "navBtn02").setActiveColor(ResourceUtils.getColor(getContext(), R.color.color_green_500));
        BottomNavigationItem item_3 = new BottomNavigationItem(R.drawable.fragmentation_ic_expandable, "navBtn03").setActiveColor(ResourceUtils.getColor(getContext(), R.color.color_orange_500));
*/
        BottomNavigationItem item_1 = new BottomNavigationItem(R.drawable.fragmentation_help, "navBtn01").setActiveColor(ResourceUtils.getColor(getContext(), R.color.app_color));
        BottomNavigationItem item_2 = new BottomNavigationItem(R.drawable.fragmentation_ic_right, "navBtn02").setActiveColor(ResourceUtils.getColor(getContext(), R.color.app_color));
        BottomNavigationItem item_3 = new BottomNavigationItem(R.drawable.fragmentation_ic_expandable, "navBtn03").setActiveColor(ResourceUtils.getColor(getContext(), R.color.app_color));




        navigationBar.addItem(item_1);
        navigationBar.addItem(item_2);
        navigationBar.addItem(item_3);
        navigationBar.setFirstSelectedPosition(0);
        navigationBar.setMode(BottomNavigationBar.MODE_DEFAULT);
        navigationBar.setBackgroundStyle(BottomNavigationBar.BACKGROUND_STYLE_RIPPLE);
        navigationBar.setTabSelectedListener(this);
        navigationBar.initialise();
    }

    private void initTopFabBtn() {
        mTopFabtn = bind(R.id.delegate_main_top_fab);
        mTopFabtn.hide();
        mTopFabtn.setOnClickListener(v -> { //点击到顶
            switch (nowPosition) {
                case 0 ://新闻模块
                    showBottomMenu();
                    mTopFabtn.hide();
                    EventBus.getDefault().post(EventBusPosKey.SCROLL_TO_TOP); //滚动到最顶
                    break;
                case 1://微信文章模块
                    showBottomMenu();
                    mTopFabtn.hide();
                    EventBus.getDefault().post(EventBusPosKey.SCROLL_TO_TOP); //滚动到最顶
                    break;
            }
        });
    }


    private void initAnimator() {
        Animator.AnimatorListener showlistener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (!isLoadingAnim) {
                    isLoadingAnim = true;
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                isLoadingAnim = false;
                barIsShowing = true;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        };

        Animator.AnimatorListener hidelistener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (!isLoadingAnim) {
                    isLoadingAnim = true;
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                isLoadingAnim = false;
                barIsShowing = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        };
        mHideAnimator = AnimatorUtils.loadBottomHideAnimator(navigationBar);
        mShowAnimator = AnimatorUtils.loadBottomShowAnimator(navigationBar);
        mHideAnimator.addListener(hidelistener);
        mShowAnimator.addListener(showlistener);
    }

    @Override
    public void onTabSelected(int position) { //选择的tab
        nowPosition = position;
        changeDelege(position);
    }

    @Override
    public void onTabUnselected(int position) {
    } //

    @Override
    public void onTabReselected(int position) {
    }


    private void changeDelege(int position) {
        BaseFragmentDelegate tempDelegate = DELEGATES.get(position);
        switch (position) {
            case 0:
                EventBus.getDefault().post(EventBusPosKey.CHANGE_BLUE_STATE_BAR);
                break;
            case 1:
                EventBus.getDefault().post(EventBusPosKey.CHANGE_GREEN_STATE_BAR);
                break;
            case 2:
                EventBus.getDefault().post(EventBusPosKey.CHANGE_ORANGE_STATE_BAR);
                break;
        }
        if (!tempDelegate.equals(nowDelege)) {
            getSupportDelegate().showHideFragment(tempDelegate, nowDelege);
            nowDelege = tempDelegate;
        }
    }

    //修改底部菜单状态 接收从NewsListDelegate接收
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBarColorStatus(EventBusPosKey config) {

        //-----------------------------返回最上按钮隐藏显示
        if (config == EventBusPosKey.HIDE_BOTTOM_TO_TOP_BOTTON) {//隐藏返回最上按钮
            mTopFabtn.hide();
        }

        if (config == EventBusPosKey.SHOW_BOTTOM_TO_TOP_BOTTON) {//显示返回最上按钮
            mTopFabtn.show();
        }


        //-----------------------------底部菜单隐藏显示
        if (config == EventBusPosKey.HIDE_BOTTOM_NAVIGATIONBAR) {//隐藏菜单
            if (!isLoadingAnim) {
                if (barIsShowing) {
                    hideBottomMenu();
                }
            }
        }
        if (config == EventBusPosKey.SHOW_BOTTOM_NAVIGATIONBAR) {//显示菜单

            if (!isLoadingAnim) {
                if (!barIsShowing) {
                    showBottomMenu();
                }
            }
        }
    }

    private Animator mHideAnimator;
    private Animator mShowAnimator;

    private void hideBottomMenu() {
        mHideAnimator.start();
    }

    private void showBottomMenu() {
        mShowAnimator.start();
    }

    @Override
    public void quit() { //退出
        mActivity.finish();
    }

    @Override
    public void notQuit() {
        ToastUtils.warning("连续点击两次退出");
    }
}
