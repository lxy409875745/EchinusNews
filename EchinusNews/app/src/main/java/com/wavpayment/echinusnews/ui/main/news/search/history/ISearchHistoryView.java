package com.wavpayment.echinusnews.ui.main.news.search.history;

public interface ISearchHistoryView {
    /** 获取历史记录完成
     * @param haveData
     */
    void getSearchHistoryDone(boolean haveData);


    /**
     * 删除历史记录完成
     */
    void delSearchHistoryDone(boolean haveData);

    /**
     * 删除所有历史记录完成
     */
    void delAllSearchHistoryDone();

}
