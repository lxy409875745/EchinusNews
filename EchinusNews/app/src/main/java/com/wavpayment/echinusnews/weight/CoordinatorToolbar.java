package com.wavpayment.echinusnews.weight;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class CoordinatorToolbar extends Toolbar {
    public CoordinatorToolbar(Context context) {
        super(context);
    }

    public CoordinatorToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CoordinatorToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }



}
