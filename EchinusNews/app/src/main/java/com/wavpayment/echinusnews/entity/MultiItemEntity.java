package com.wavpayment.echinusnews.entity;

public interface MultiItemEntity extends com.chad.library.adapter.base.entity.MultiItemEntity {
    int getItemType();
}
