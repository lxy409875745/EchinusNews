package com.wavpayment.echinusnews.comment.callback;

public enum  EventBusPosKey {

    HIDE_BOTTOM_NAVIGATIONBAR, //隐藏下标栏
    SHOW_BOTTOM_NAVIGATIONBAR, //显示下标栏
    HIDE_BOTTOM_TO_TOP_BOTTON,//隐藏返回最顶按钮
    SHOW_BOTTOM_TO_TOP_BOTTON,//显示返回最按钮
    CHANGE_BLUE_STATE_BAR,//蓝色state Bar
    CHANGE_GREEN_STATE_BAR,//绿色state Bar
    CHANGE_ORANGE_STATE_BAR,//橘色state Bar

    SCROLL_TO_TOP,//滚动到顶部

    NET_STATUS_CHANGE//网络状态变化

}
