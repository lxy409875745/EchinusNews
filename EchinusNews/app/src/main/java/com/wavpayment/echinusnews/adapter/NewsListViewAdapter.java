package com.wavpayment.echinusnews.adapter;


import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.entity.NewsBean;

import java.util.ArrayList;

/**
 * Created by lenovo on 2018/5/14.
 */

public class NewsListViewAdapter extends BaseMultiItemQuickAdapter<NewsBean, NewsListViewAdapter.NewslIstViewholder> {
    public static final int ITEM_TYPE_NORMAL = 0x00000000;
    public static final int ITEM_TYPE_BIG = 0x00000001;
    public static final int ITEM_TYPE_BANNER = 0x00000002;

    private Context mContext;
    private RequestOptions mImageOption;
    private DrawableCrossFadeFactory drawableCrossFadeFactory;

    public NewsListViewAdapter(Context context) {
        super(new ArrayList<>());
        mContext = context;
        //設置好Glide選項
        mImageOption = new RequestOptions();
        mImageOption.error(R.drawable.error_img);
        mImageOption.placeholder(R.drawable.loading_img);
        //圖片加載動畫

        drawableCrossFadeFactory = new DrawableCrossFadeFactory.Builder(300).setCrossFadeEnabled(true).build();


        addItemType(ITEM_TYPE_NORMAL, R.layout.item_news_normal);
        addItemType(ITEM_TYPE_BIG, R.layout.item_news_big);
        addItemType(ITEM_TYPE_BANNER, R.layout.item_news_banner);
    }

    @Override
    protected void convert(NewslIstViewholder helper, NewsBean item) {
        Log.i(TAG, "convert: itemType = " + item.getItemType());
        switch (helper.getItemViewType()) {
            case ITEM_TYPE_NORMAL:
                loadNormalLayout(helper, item);
                break;
            case ITEM_TYPE_BIG:
                loadBigLayout(helper, item);
                break;
            case ITEM_TYPE_BANNER:
                //    loadBannerLayout (helper,item);
                loadBigLayout(helper, item);//暂时用大图代替
                break;
        }


    }

    private void loadNormalLayout(NewslIstViewholder helper, NewsBean item) {
        ImageView imageView = helper.getView(R.id.item_news_img);
        TextView textView = helper.getView(R.id.item_news_tv_title);
        ViewCompat.setTransitionName(imageView, String.valueOf(helper.getLayoutPosition()) + "_image");
        ViewCompat.setTransitionName(textView, String.valueOf(helper.getLayoutPosition()) + "_tv");
        helper.setText(R.id.item_news_tv_title, item.getTitle());
        helper.setText(R.id.item_news_tv_config, item.getDescription() + "   " + item.getCtime());
        Glide.with(mContext)
                .load(item.getPicUrl())
                .apply(mImageOption)
                .transition(DrawableTransitionOptions.with(drawableCrossFadeFactory))
                .into(imageView);
    }

    private void loadBigLayout(NewslIstViewholder helper, NewsBean item) {
        ImageView imageView = helper.getView(R.id.item_news_img);
        TextView textView = helper.getView(R.id.item_news_tv_title);
        ViewCompat.setTransitionName(imageView, String.valueOf(helper.getLayoutPosition()) + "_image");
        ViewCompat.setTransitionName(textView, String.valueOf(helper.getLayoutPosition()) + "_tv");
        helper.setText(R.id.item_news_tv_title, item.getTitle());
        helper.setText(R.id.item_news_tv_config, item.getDescription() + "   " + item.getCtime());
        Glide.with(mContext)
                .load(item.getPicUrl())
                .apply(mImageOption)
                .transition(DrawableTransitionOptions.with(drawableCrossFadeFactory))
                .into(imageView);
    }

    private void loadBannerLayout(NewslIstViewholder helper, NewsBean item) {

    }

    public class NewslIstViewholder extends BaseViewHolder {
        public NewslIstViewholder(View view) {
            super(view);
        }
    }
}
