package com.wavpayment.echinusnews.ui.main.news.search.history;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.adapter.SearchHistoryGridViewAdapter;
import com.wavpayment.echinusnews.base.BaseFragmentDelegate;
import com.wavpayment.echinusnews.weight.completlayout.CompleteLayout;
import com.wavpayment.echinusnews.weight.completlayout.CompleteLayoutAdapter;

public class SearchHistoryDelegate extends BaseFragmentDelegate implements ISearchHistoryView {
    private CompleteLayout mCompleteLayout;//历史item
    private View mDeleteBtn;//删除
    private TextView mMessageTv;//没有历史记录时候的提示


    private CompleteLayoutAdapter mAdapter;
    private SearchHistoryPresenterImp mPresenter;
    private IHistoryItemClickCallback mItemClickCallback; //回调点击数据


    private GridView mGridView;//推荐新闻 //默认随便一个标签拿新闻
    private SearchHistoryGridViewAdapter mGridAdapter;

    @Override
    public Object setLayout() {
        return R.layout.delegate_search_history;
    }

    @Override
    public void onBindeView(View rootView, @Nullable Bundle savedInstanceState) {
        initViews();
        initPresenter();
        showHistory();
        showRecommentNews();

    }



    private void initPresenter() {
        mPresenter = new SearchHistoryPresenterImp(getContext(), mAdapter,mGridAdapter, this);
    }

    private void initViews() {
        mCompleteLayout = bind(R.id.delegate_search_history_completeLayout);
        mCompleteLayout.setTextSize(16);
        mCompleteLayout.setTextPadding(28);
        mCompleteLayout.setAdapter(mAdapter = new CompleteLayoutAdapter(getContext()));
        mDeleteBtn = bind(R.id.delegate_search_history_delete_btn);
        mMessageTv = bind(R.id.delegate_search_history_msg_tv);

        mAdapter.setOnItemClickListener((adapter, textView, text, position) -> { //点击历史记录
            mItemClickCallback.onItemClick(text);
        });

        mDeleteBtn.setOnClickListener(v -> { //点击删除
            mAdapter.removeAll();
            mDeleteBtn.setVisibility(View.INVISIBLE);
            mMessageTv.setVisibility(View.VISIBLE);
            mPresenter.delAllHistory();
        });


        mGridView = bind(R.id.delegate_search_history_gridView);
        mGridView.setNumColumns(2);//两列
        mGridView.setAdapter(mGridAdapter = new SearchHistoryGridViewAdapter(getContext()));

    }


    /**
     * 设置回调
     *
     * @param callback
     */
    public void setHistoryItemClickCallback(IHistoryItemClickCallback callback) {
        mItemClickCallback = callback;
    }

    /**
     * 显示推荐的新闻
     */
    private void showRecommentNews() {
        mPresenter.showRecommendNews();
    }
    /**
     * 搜索历史记录
     */
    public void showHistory() {
        mPresenter.showHistory();
    }

    /**
     * 添加历史记录
     *
     * @param searchWord
     */
    public void addSearchWord(String searchWord) {
        mPresenter.addHistory(searchWord);
    }


    /**
     * 获取历史记录结束
     *
     * @param haveData 是否有数据
     */
    @Override
    public void getSearchHistoryDone(boolean haveData) {
        if (haveData) { //当前有数据
            mMessageTv.setVisibility(View.INVISIBLE);
            mDeleteBtn.setVisibility(View.VISIBLE);
        } else {
            mMessageTv.setVisibility(View.VISIBLE);
            mDeleteBtn.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * 删除单个历史记录的回调 暂时无用
     *
     * @param haveData
     */
    @Override
    public void delSearchHistoryDone(boolean haveData) {
        //暂时无用
    }


    @Override
    public void delAllSearchHistoryDone() {
        mDeleteBtn.setVisibility(View.INVISIBLE);
    }


    public interface IHistoryItemClickCallback {
        void onItemClick(String searchWrodHistory);
    }






}
