package com.wavpayment.echinusnews.weight;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * 一般的GridView 和ScrollView冲突 因为gridview也有滚轮 需要自定义
 */
public class ScrollGridView extends GridView {

    public ScrollGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollGridView(Context context) {
        super(context);
    }

    public ScrollGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(
                Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }

}
