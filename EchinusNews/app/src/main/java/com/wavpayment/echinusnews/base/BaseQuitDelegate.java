package com.wavpayment.echinusnews.base;

/**
 * 关闭应用程序
 * Created by Administrator on 2018/5/11.
 */



public abstract class BaseQuitDelegate extends BaseFragmentDelegate {

    @Override
    public boolean onBackPressedSupport() {
        mActivity.finish();
        return true;
    }
}
