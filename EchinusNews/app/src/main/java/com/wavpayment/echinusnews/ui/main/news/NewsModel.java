package com.wavpayment.echinusnews.ui.main.news;

import android.content.Context;

import com.wavpayment.echinusnews.comment.db.DBManager;
import com.wavpayment.echinusnews.comment.network.URLManager;
import com.wavpayment.echinusnews.entity.NewsTypeBean;

import java.util.List;

/**
 * Created by Administrator on 2018/5/12.
 */

public class NewsModel {

    private Context mContext;

    public NewsModel(Context mContext) {
        this.mContext = mContext;
    }

    public List<NewsTypeBean> getNewsTypes (){
        return DBManager.getDaoSession(mContext).getNewsTypeBeanDao().queryBuilder().build().list();
    }

    public void initDefualNewsTypes(){
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().deleteAll();

        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("社会热点", URLManager.NEWS_SOCIAL,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("国内要闻", URLManager.NEWS_GUONEI,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("国际要闻", URLManager.NEWS_WORLD,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("娱乐花边", URLManager.NEWS_HUABIAN,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("体育动态", URLManager.NEWS_TIYU,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("NBA赛事", URLManager.NEWS_NBA,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("足球资讯", URLManager.NEWS_FOOTBALL,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("数码科技", URLManager.NEWS_KEJI,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("互联网", URLManager.NEWS_STARTUP,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("苹果动态", URLManager.NEWS_APPLE,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("军事资讯", URLManager.NEWS_MILITARY,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("移动互联网", URLManager.NEWS_MOBILE,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("旅游周边", URLManager.NEWS_TRAVEL,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("健康知识", URLManager.NEWS_HEALTH,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("奇闻趣事", URLManager.NEWS_QIWEN,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("亮眼美女", URLManager.NEWS_MEINV,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("VR动态", URLManager.NEWS_VR,true));
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().insert(new NewsTypeBean("IT资讯", URLManager.NEWS_IT,true));
    }


    public void upDataNewsTypeBean(NewsTypeBean bean){
        DBManager.getDaoSession(mContext).getNewsTypeBeanDao().update(bean);
    }


}
