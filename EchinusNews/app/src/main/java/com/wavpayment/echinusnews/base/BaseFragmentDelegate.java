package com.wavpayment.echinusnews.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.wavpayment.echinusnews.R;

import me.yokeyword.fragmentation.ExtraTransaction;
import me.yokeyword.fragmentation.ISupportFragment;
import me.yokeyword.fragmentation.SupportFragmentDelegate;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

/**
 * Created by Administrator on 2018/5/9.
 */

public abstract class BaseFragmentDelegate extends Fragment implements ISupportFragment {

    private final SupportFragmentDelegate DELEGATE = new SupportFragmentDelegate(this);

    public abstract Object setLayout();
    public abstract void onBindeView(View rootView,@Nullable Bundle savedInstanceState);

    private View mRootView = null;
    protected AppCompatActivity mActivity = null;



    public <T extends View> T bind (@IdRes int id){
        if (mRootView != null){
            return mRootView.findViewById(id);
        }
        throw new NullPointerException(getResources().getString(R.string.exception_nullPointException_rootview));
    }
    public void start (BaseFragmentDelegate fragmentDelegate){
        if (fragmentDelegate != null){
            DELEGATE.start(fragmentDelegate);
        }else {
            throw new NullPointerException(getResources().getString(R.string.exception_nullPointException_fragmentDelegate));
        }
    }

    public void start(BaseFragmentDelegate fragmentDelegate,int lunchMod){
        if (fragmentDelegate != null){
            DELEGATE.start(fragmentDelegate,lunchMod);
        }else {
            throw new NullPointerException(getResources().getString(R.string.exception_nullPointException_fragmentDelegate));
        }
    }


    /** 获取父辈的fragmentdelegate
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T extends BaseFragmentDelegate> T getParentDelegate() {
        return (T) getParentFragment();
    }

    /** 隐藏键盘
     * @param view
     */
    public  void hideKeyboard(EditText view) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
        }
    }

    /** 显示键盘
     * @param view
     */
    public void showKeyboard(EditText view){
        InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(view, 0);
    }




    public AppCompatActivity  getBaseActivity(){
        return mActivity;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView;
        Object layoutObject = setLayout();
        if (layoutObject instanceof Integer){
            int viewId = (int)layoutObject;
            rootView = inflater.inflate(viewId,container,false);
        }else if (layoutObject instanceof View ){
            rootView = (View) layoutObject;
        }else {
            throw new ClassCastException(getResources().getString(R.string.exception_classCastException_setLayout));
        }

        mRootView = rootView;
        onBindeView(mRootView,savedInstanceState);


        return mRootView;
    }










    public BaseActivityDelegate getmActivity() {
        return (BaseActivityDelegate) mActivity;
    }

    @Override
    public SupportFragmentDelegate getSupportDelegate() {
        return DELEGATE;
    }

    @Override
    public ExtraTransaction extraTransaction() {
        return DELEGATE.extraTransaction();
    }

    @Override
    public void enqueueAction(Runnable runnable) {
        DELEGATE.enqueueAction(runnable);
    }

    @Override
    public void post(Runnable runnable) {
        DELEGATE.post(runnable);
    }

    @Override
    public void onEnterAnimationEnd(@Nullable Bundle savedInstanceState) {
        DELEGATE.onEnterAnimationEnd(savedInstanceState);
    }

    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        DELEGATE.onLazyInitView(savedInstanceState);
    }

    @Override
    public void onSupportVisible() {
        DELEGATE.onSupportVisible();
    }

    @Override
    public void onSupportInvisible() {
        DELEGATE.onSupportInvisible();
    }

    @Override
    public boolean isSupportVisible() {
        return DELEGATE.isSupportVisible();
    }

    @Override
    public FragmentAnimator onCreateFragmentAnimator() {
        return DELEGATE.onCreateFragmentAnimator();
    }

    @Override
    public FragmentAnimator getFragmentAnimator() {
        return DELEGATE.getFragmentAnimator();
    }

    @Override
    public void setFragmentAnimator(FragmentAnimator fragmentAnimator) {
        DELEGATE.setFragmentAnimator(fragmentAnimator);
    }

    @Override
    public void setFragmentResult(int resultCode, Bundle bundle) {
        DELEGATE.setFragmentResult(resultCode,bundle);
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        DELEGATE.onFragmentResult(requestCode,resultCode,data);
    }

    @Override
    public void onNewBundle(Bundle args) {
        DELEGATE.onNewBundle(args);
    }

    @Override
    public void putNewBundle(Bundle newBundle) {
        DELEGATE.putNewBundle(newBundle);
    }

    @Override
    public boolean onBackPressedSupport() {
        return DELEGATE.onBackPressedSupport();
    }




    //---------------------------------------------fragment----------------------------------------



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DELEGATE.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        DELEGATE.onAttach((Activity) context);
        mActivity = (AppCompatActivity) DELEGATE.getActivity();
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DELEGATE.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        DELEGATE.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        DELEGATE.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        DELEGATE.onPause();
    }

    @Override
    public void onDestroyView() {
        DELEGATE.onDestroyView();
        super.onDestroyView();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        DELEGATE.onHiddenChanged(hidden);
    }

    @Override
    public void onDestroy() {
        DELEGATE.onDestroy();
        super.onDestroy();
    }
}
