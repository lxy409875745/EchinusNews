package com.wavpayment.echinusnews.adapter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.flyco.tablayout.SlidingTabLayout;
import com.wavpayment.echinusnews.base.BaseFragmentDelegate;
import com.wavpayment.echinusnews.entity.NewsTypeBean;
import com.wavpayment.echinusnews.ui.main.news.newslist.NewsListDelegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2018/5/12.
 */

public class NewsPagerAdapter extends FragmentPagerAdapter {

    private List<NewsTypeBean> titles = new ArrayList<>();
    private HashMap<String,NewsListDelegate> fragmentMap = new HashMap<>();
    private BaseFragmentDelegate mParentFragmentDelegate; //给viewpager中未进栈的fragment传入父fragment viewpager中的fragment打开新页面时必须用父fragment去打开
    public NewsPagerAdapter(BaseFragmentDelegate parentFragmentDelegate, FragmentManager fm) {
        super(fm);
        mParentFragmentDelegate = parentFragmentDelegate;

    }
    public void addData(List<NewsTypeBean> data){
        titles.addAll(data);
        mTabLayout.notifyDataSetChanged();
        notifyDataSetChanged();
    }



    @Override
    public Fragment getItem(int position) {
        NewsListDelegate tempFragment = fragmentMap.get(titles.get(position));
        if (tempFragment == null){
            Bundle bundle = new Bundle();
            bundle.putParcelable("newsType",titles.get(position));
            tempFragment = new NewsListDelegate();
            tempFragment.setParantDelegateFragment(mParentFragmentDelegate);
            tempFragment.setArguments(bundle);
            fragmentMap.put(titles.get(position).getTitle(),tempFragment);
        }
        return tempFragment;
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position).getTitle();
    }




    private SlidingTabLayout mTabLayout;
    public void addTabLayout(SlidingTabLayout tablayout) {
        mTabLayout = tablayout;
    }





}
