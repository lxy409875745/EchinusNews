package com.wavpayment.echinusnews.ui.main.wechat;

import android.content.Context;

import com.wavpayment.echinusnews.comment.callback.CallbackConfig;
import com.wavpayment.echinusnews.comment.callback.CallbackManager;
import com.wavpayment.echinusnews.comment.db.DBManager;
import com.wavpayment.echinusnews.comment.db.NewsBeanDao;
import com.wavpayment.echinusnews.comment.network.NetworkHandler;
import com.wavpayment.echinusnews.comment.network.URLManager;
import com.wavpayment.echinusnews.entity.NewsBean;

import java.util.List;

public class WechatModel implements IWechatModel {
    private Context mContext;

    public WechatModel(Context context) {
        mContext = context;
    }

    /**
     * 从网络获取数据
     *
     * @param nowPage
     * @param pageNumber
     */
    @Override
    public void requestDataFromNet(int nowPage, int pageNumber) {
        NetworkHandler.getWechatNews(nowPage,pageNumber);
    }

    /**
     * 从本地获取数据
     *
     * @param nowPage
     * @param pageNumber
     */
    @Override
    public void requestDataFromLocal(int nowPage, int pageNumber) {
        String type = URLManager.WECHAT_URL;//设置类型
        List<NewsBean> localData = queryNewsBeanByNewsType(type, nowPage, pageNumber);
        CallbackManager.getInstance().getCallback(CallbackConfig.WECHAT_NEWS_LOCAL).excuteCallback(localData);
    }


    //---------------------------------------------DB操作---------------------------------------------------

    //插入一條數據
    public void insertData(NewsBean newsBean) {
        DBManager.getDaoSession(mContext).getNewsBeanDao().insert(newsBean);
    }

    //插入一套數據
    public void insertData(List<NewsBean> newsBeans) {
        Iterable<NewsBean> iterable = () -> newsBeans.iterator();
        DBManager.getDaoSession(mContext).getNewsBeanDao().insertInTx(iterable);
    }

    public void deleteAll() {
        DBManager.getDaoSession(mContext).getNewsBeanDao().deleteAll();
    }

    //刪除某一分類的新聞
    public void deleteByNewsType(String newsType) {
        DBManager.getDaoSession(mContext).getNewsBeanDao().queryBuilder()
                .where(NewsBeanDao.Properties.NewsType
                        .eq(newsType)).buildDelete()
                .executeDeleteWithoutDetachingEntities();
    }

    //獲取某個分類中 最開始的那一條數據
    public List<NewsBean> queryNewsBeanByRequestTime(String newsType) {
        //select * from om_meeting_schedule s where s.is_use=1
        //ORDER BY ABS(NOW() - s.meeting_begin_date) ASC
        //limit 1
        List<NewsBean> newsBeans = DBManager.getDaoSession(mContext).getNewsBeanDao().queryBuilder()
                .where(NewsBeanDao.Properties.NewsType
                        .eq(newsType))
                .orderAsc(NewsBeanDao.Properties.RequestTime)
                .limit(1)
                .list();
        return newsBeans;
    }

    //獲取分類數據
    public List<NewsBean> queryNewsBeanByNewsType(String newsType, int nowPage, int pageNumber) {
        List<NewsBean> newsBeans = DBManager.getDaoSession(mContext).getNewsBeanDao().queryBuilder()
                .where(NewsBeanDao.Properties.NewsType
                        .eq(newsType))
                .offset(nowPage * pageNumber)
                .limit(pageNumber)
                .list();
        return newsBeans;
    }

    //獲取某個分類的數量
    public long queryNewsBeanNewsTypeCount(String newsType) {
        long count = DBManager.getDaoSession(mContext).getNewsBeanDao().queryBuilder()
                .where(NewsBeanDao.Properties.NewsType
                        .eq(newsType))
                .buildCount().count();
        return count;
    }


}
