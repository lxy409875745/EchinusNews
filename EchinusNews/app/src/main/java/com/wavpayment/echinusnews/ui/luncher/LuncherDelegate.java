package com.wavpayment.echinusnews.ui.luncher;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.Animation;
import com.wavpayment.echinusnews.R;

import com.wavpayment.echinusnews.base.BaseQuitDelegate;
import com.wavpayment.echinusnews.comment.callback.CallbackConfig;
import com.wavpayment.echinusnews.comment.callback.CallbackManager;
import com.wavpayment.echinusnews.comment.local.SharePreferenceKey;
import com.wavpayment.echinusnews.ui.luncher.welcome.WelcomeActivity;
import com.wavpayment.echinusnews.ui.main.MainDelegate;
import com.wavpayment.echinusnews.utils.AnimationUtils;
import com.wavpayment.echinusnews.utils.SharePreferenceUtil;

/**
 * Created by Administrator on 2018/5/9.
 */

public class LuncherDelegate extends BaseQuitDelegate {


    private Handler handler = new Handler();


    @Override
    public Object setLayout() {
        return R.layout.delegate_luncher;
    }

    private boolean isFristOpen = false;
    @Override
    public void onBindeView(View rootView, @Nullable Bundle savedInstanceState) {
        initSettingData();
        initCallback();
        initAnim();
        bind(R.id.delegate_luncher_tv).startAnimation(left2MidAnim);
        bind(R.id.delegate_luncher_iv).startAnimation(right2MidAnim);
        handler.postDelayed(() ->{
            if (isFristOpen){
                startActivity(new Intent(getBaseActivity(), WelcomeActivity.class));
            }
            else {start(new MainDelegate());}}
                ,2000);
    }

    /**
     * 初始化一些配置数据
     */
    private void initSettingData(){
        //-----------------------------------新闻详情页的字体
        isFristOpen = (boolean) SharePreferenceUtil.get(getContext(),SharePreferenceKey.SPKEY_APP_FRIST_OPEN,true);
        int frontSize = (int) SharePreferenceUtil.get(getContext(),SharePreferenceKey.SPKEY_NEWS_DETAIL_FRONT_SIZE,-1);
        if (frontSize == -1)
            SharePreferenceUtil.put(getContext(),SharePreferenceKey.SPKEY_NEWS_DETAIL_FRONT_SIZE,1);//默认为正常字体
    }




    private void initCallback() {
        CallbackManager.getInstance().addCallback(CallbackConfig.WELCOME_END, value ->  start(new MainDelegate()));
        if (isFristOpen)
            SharePreferenceUtil.put(getContext(), SharePreferenceKey.SPKEY_APP_FRIST_OPEN,false);
    }

    private Animation left2MidAnim;
    private Animation right2MidAnim;
    private void initAnim(){
        left2MidAnim =  AnimationUtils.loadAnimation(getContext(),R.anim.left_to_mid_with_alpha);
        right2MidAnim = AnimationUtils.loadAnimation(getContext(),R.anim.right_to_mid_with_alpha);
    }
}
