package com.wavpayment.echinusnews.ui.main.news;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.transition.Fade;
import android.util.Log;
import android.view.View;

import com.flyco.tablayout.SlidingTabLayout;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.adapter.NewsPagerAdapter;
import com.wavpayment.echinusnews.base.BaseFragmentDelegate;
import com.wavpayment.echinusnews.comment.anim.SharedElementTransation;
import com.wavpayment.echinusnews.ui.main.news.search.SearchDelegate;


/**
 * Created by Administrator on 2018/5/11.
 */

public class NewsDelegate extends BaseFragmentDelegate {
    private static final String TAG = "NewsDelegate";

    @Override
    public Object setLayout() {
        return R.layout.delegate_news;
    }

    @Override
    public void onBindeView(View rootView, @Nullable Bundle savedInstanceState) {
        initSerchView();
        initViewpager();
        initTabLayout();
        initPresenter();
    }

    private View searchBtn;

    private void initSerchView() {

        searchBtn = bind(R.id.item_serch_bar_serBtn);
        ViewCompat.setTransitionName(searchBtn, getString(R.string.transation_search_search_edt));
        searchBtn.setOnClickListener(v -> { //
            startSearchDelegate();
        });
    }

    private NewsPagerAdapter mAdapter;
    private ViewPager mViewpager;

    private void initViewpager() {
        mViewpager = bind(R.id.delegate_news_viewpager);
        mAdapter = new NewsPagerAdapter(getParentDelegate(),getChildFragmentManager());
        mViewpager.setAdapter(mAdapter);
    }

    private SlidingTabLayout mTablayout;

    private void initTabLayout() {
        mTablayout = bind(R.id.delegate_news_tabLayout);
        mAdapter.addTabLayout(mTablayout);
        mTablayout.setViewPager(mViewpager);
    }

    private NewsPresenter mNewsPresenter;

    private void initPresenter() {
        mNewsPresenter = new NewsPresenter(getContext(), mAdapter);
    }

    private void startSearchDelegate() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            Log.i(TAG, "startSearchDelegate: > LOLLIPOP");
            SearchDelegate searchDelegate =  new SearchDelegate();
            getParentDelegate().setExitTransition(new Fade());
            searchDelegate.setEnterTransition(new Fade());
            searchDelegate.setSharedElementReturnTransition(new SharedElementTransation());
            searchDelegate.setSharedElementEnterTransition(new SharedElementTransation());
            // 25.1.0以下的support包,Material过渡动画只有在进栈时有,返回时没有;
            // 25.1.0+的support包，SharedElement正常
            getParentDelegate().getSupportDelegate().extraTransaction()
                    .addSharedElement(searchBtn,getString(R.string.transation_search_search_edt))
                    .start(searchDelegate);


        } else {
            Log.i(TAG, "startSearchDelegate: < LOLLIPOP");
            getParentDelegate().getSupportDelegate().start(new SearchDelegate());
        }
    }

}
