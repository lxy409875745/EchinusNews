package com.wavpayment.echinusnews.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.entity.NewsBean;

public class SearchResultNewsListAdapter extends BaseQuickAdapter<NewsBean,SearchResultNewsListAdapter.SearchResultNewsListViewHolder> {

    private Context mContext;
    private RequestOptions mImageOption;
    private DrawableCrossFadeFactory drawableCrossFadeFactory;
    public SearchResultNewsListAdapter(Context context) {
        super(R.layout.item_news_normal);
        mContext = context;
        //設置好Glide選項
        mImageOption = new RequestOptions();
        mImageOption.error(R.drawable.error_img);
        mImageOption.placeholder(R.drawable.loading_img);
        //圖片加載動畫
        drawableCrossFadeFactory = new DrawableCrossFadeFactory.Builder(300).setCrossFadeEnabled(true).build();


    }

    @Override
    protected void convert(SearchResultNewsListViewHolder helper, NewsBean item) {
        ImageView imageView = helper.getView(R.id.item_news_img);
        helper.setText(R.id.item_news_tv_title,item.getTitle());
        helper.setText(R.id.item_news_tv_config,item.getDescription()+"   "+item.getCtime());
        Glide.with(mContext)
                .applyDefaultRequestOptions(mImageOption)
                .load(item.getPicUrl())
                .transition(DrawableTransitionOptions.with(drawableCrossFadeFactory))
                .into(imageView);

    }
    public class SearchResultNewsListViewHolder extends BaseViewHolder{
        public SearchResultNewsListViewHolder(View view) {
            super(view);
        }
    }
}
