package com.wavpayment.echinusnews.ui.main.news.search.result.resultnews;

import android.content.Context;

import com.wavpayment.echinusnews.comment.network.NetworkHandler;
import com.wavpayment.echinusnews.entity.NewsTypeBean;

public class SearchResultNewsListModelImp implements ISearchResultNewsListModel {
    private Context mContext;
    private NewsTypeBean mNewsTypeBean;


    public SearchResultNewsListModelImp(Context context, NewsTypeBean newsTypeBean) {
        this.mContext = context;
        this.mNewsTypeBean = newsTypeBean;
    }



    @Override
    public void requestNewsBySearch(int nowPage, int pageNumber, String searchWord) {
        NetworkHandler.getNewsByKeyWord(mNewsTypeBean, nowPage, pageNumber,searchWord);
    }
}
