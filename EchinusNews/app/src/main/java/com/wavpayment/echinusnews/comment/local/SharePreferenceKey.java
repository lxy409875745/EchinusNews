package com.wavpayment.echinusnews.comment.local;

public class SharePreferenceKey {
    public static final String SPKEY_APP_FRIST_OPEN = "app_frist_open";//app是否第一次打开 ture 第一次打开     false
    public static final String SPKEY_NEWS_DETAIL_FRONT_SIZE = "news_detail_front_size";//详情页的字体大小 1 小字体  2中字体 3大字体
}
