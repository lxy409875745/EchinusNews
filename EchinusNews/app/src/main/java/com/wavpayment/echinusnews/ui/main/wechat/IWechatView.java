package com.wavpayment.echinusnews.ui.main.wechat;

public interface IWechatView {
    void refreshDone(boolean haveData,int count);
    void loadmoreDone(boolean haveData,int cound);

    void refreshFail(Exception e);
    void loadmoreFail(Exception e);
}
