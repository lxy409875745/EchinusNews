package com.wavpayment.echinusnews.utils;

import com.vondear.rxtool.view.RxToast;

public class ToastUtils {

    public static void normal(String message){ RxToast.normal(message); }
    public static void success(String message){
        RxToast.success(message);
    }
    public static void warning(String message){
        RxToast.warning(message);
    }
    public static void error(String message){
        RxToast.error(message);
    }


}
