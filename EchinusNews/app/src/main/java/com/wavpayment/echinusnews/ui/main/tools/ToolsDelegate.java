package com.wavpayment.echinusnews.ui.main.tools;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.base.BaseFragmentDelegate;

/**
 * Created by Administrator on 2018/5/11.
 */

public class ToolsDelegate extends BaseFragmentDelegate {
    @Override
    public Object setLayout() {
        return R.layout.delegate_tools;
    }

    @Override
    public void onBindeView(View rootView, @Nullable Bundle savedInstanceState) {

    }
}
