package com.wavpayment.echinusnews.comment.network;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;
import com.lzy.okgo.request.GetRequest;
import com.lzy.okgo.request.PostRequest;
import com.lzy.okgo.request.base.Request;

import java.util.HashMap;

/**
 * Created by Administrator on 2018/5/12.
 */

public class Network {

    private static class Handler {
        private static Network INSTANSE = new Network();
    }


    private Network (){}

    public static Network getInstanse(){
        return Handler.INSTANSE;
    }


    public Request<String,GetRequest<String>>get(String url){
        return OkGo.<String>get(url)
                .tag(this);
    }
    public Request<String,GetRequest<String>>get(String url, HttpParams params){
        return OkGo.<String>get(url)
                .tag(this)
                .params(params);
    }

    public Request<String,GetRequest<String>>get(String url, HashMap<String,String> params){
        return OkGo.<String>get(url)
                .tag(this)
                .params(params);
    }

    public Request<String,GetRequest<String>>get(String url, HttpHeaders headers){
        return OkGo.<String>get(url)
                .tag(this)
                .headers(headers);
    }


    public Request<String,GetRequest<String>>get(String url, HttpHeaders httpHeaders,HttpParams params){
        return OkGo.<String>get(url)
                .tag(this)
                .headers(httpHeaders)
                .params(params);
    }

    public Request<String,GetRequest<String>>get(String url, HttpHeaders httpHeaders,HashMap<String,String> params){
        return OkGo.<String>get(url)
                .tag(this)
                .headers(httpHeaders)
                .params(params);
    }

    public Request<String,PostRequest<String>>post(String url){
        return OkGo.<String>post(url)
                .tag(this);
    }
    public Request<String,PostRequest<String>>post(String url, HttpParams params){
        return OkGo.<String>post(url)
                .tag(this)
                .params(params);
    }


    public Request<String,PostRequest<String>>post(String url, HashMap<String,String> params){
        return OkGo.<String>post(url)
                .tag(this)
                .params(params);
    }



    public Request<String,PostRequest<String>>post(String url, HttpHeaders headers){
        return OkGo.<String>post(url)
                .tag(this)
                .headers(headers);
    }

    public Request<String,PostRequest<String>>post(String url, HttpHeaders httpHeaders,HttpParams params){
        return OkGo.<String>post(url)
                .tag(this)
                .headers(httpHeaders)
                .params(params);
    }


    public Request<String,PostRequest<String>>post(String url, HttpHeaders httpHeaders,HashMap<String,String> params){
        return OkGo.<String>post(url)
                .tag(this)
                .headers(httpHeaders)
                .params(params);
    }


}
