package com.wavpayment.echinusnews.ui.main.wechat;

import android.content.Context;
import android.os.Handler;

import com.lxy.spone.config.Spone;
import com.wavpayment.echinusnews.adapter.WechatListAdapter;
import com.wavpayment.echinusnews.comment.callback.CallbackConfig;
import com.wavpayment.echinusnews.comment.callback.CallbackManager;
import com.wavpayment.echinusnews.entity.NewsBean;
import com.wavpayment.echinusnews.entity.NewsResultBean;
import com.wavpayment.echinusnews.utils.NetUtils;

import java.util.Date;
import java.util.List;
import java.util.logging.LogRecord;

public class WechatPresenter implements IWechatPresenter {
    private Context mContext;
    private IWechatView mView;
    private IWechatModel mModel;
    private WechatListAdapter mAdapter;

    private static final int MAX_PAGE = 100;//最大頁數
    private int nowPage = 1;//当前页数
    private static final int PAGE_NUMBER = 20;//每一页条数
    private boolean isRefreshing = false;
    private boolean isLoadDataFromLocal = false; //判斷數據是否從本地獲取
    private Handler mHandle;

    public WechatPresenter(Context context, WechatListAdapter adapter, Handler handler,IWechatView view) {
        mContext = context;
        mAdapter = adapter;
        mView = view;
        initCallback();
        initModel();
        mHandle = handler;
    }

    private void initModel() {
        mModel = new WechatModel(mContext);
    }

    //----------------------------------------------------------------回调
    private void initCallback() {
        CallbackManager.getInstance().addCallback(CallbackConfig.WECHAT_NEWS_NET, value -> {  //---------------------------------------从网络获取的文章
            NewsResultBean resultBean = (NewsResultBean) value;
            if (resultBean != null && resultBean.getNewslist().size() > 0) { //返回成功
                if (isRefreshing) {
                    int dataCount = 0;//数据
                    NewsBean requestNewsBean = resultBean.getNewslist().get(0);
                    List<NewsBean> localNewsBeans = mModel.queryNewsBeanByRequestTime(requestNewsBean.getNewsType()); //获取微信分类的新闻
                    if (localNewsBeans != null && localNewsBeans.size() > 0) {//當前本地有該類型數據
                        NewsBean localNewsBean = localNewsBeans.get(0);
                        if (localNewsBean.getTitle().equals(requestNewsBean.getTitle())) {//當本地數據中最旧的一條數據比對更新下來的數據相等時 判定當前網絡數據沒有更新
                            // List<NewsBean> localData = mModel.queryNewsBeanByNewsType(requestNewsBean.getNewsType(), nowPage, PAGE_NUMBER); 不需要多余查询
                            mAdapter.setNewData(resultBean.getNewslist());
                            isLoadDataFromLocal = true;
                            dataCount = resultBean.getNewslist().size();
                        } else {
                            mModel.deleteByNewsType(requestNewsBean.getNewsType());//清除該分類的所有本地新聞數據
                            addDataFromRequest(isRefreshing, resultBean.getNewslist()); //添加新的新闻数据
                            dataCount = resultBean.getNewslist().size();
                        }
                    } else {
                        addDataFromRequest(isRefreshing, resultBean.getNewslist());
                        dataCount = resultBean.getNewslist().size();
                    }
                    refreshDone(dataCount);
                } else {
                    addDataFromRequest(isRefreshing, resultBean.getNewslist());
                    loadMoreDone(resultBean.getNewslist().size());
                }
                nowPage++;
            } else {
                if (isRefreshing) {
                    refreshFail(new Exception("刷新失败"));
                } else {
                    loadMoreFail(new Exception("加载失败"));
                }
            }
            resetRefreshFlag();
        });

        CallbackManager.getInstance().addCallback(CallbackConfig.WECHAT_NEWS_LOCAL, value -> { //-------------------------------------------从本地获取的微信文章
            List<NewsBean> localNewsBeans = (List<NewsBean>) value;
            if (isRefreshing) {
                if (localNewsBeans != null && localNewsBeans.size() > 0) { //如果当前有本地目录
                    isLoadDataFromLocal = true;
                    mHandle.post(()->   mAdapter.setNewData(localNewsBeans));
                    nowPage++;
                    resetRefreshFlag();
                    refreshDone(localNewsBeans.size());
                } else { //如果当前没有该数据 则尝试从网络获取
                    mModel.requestDataFromNet(nowPage, PAGE_NUMBER);
                }
            } else {
                if (localNewsBeans != null && localNewsBeans.size() > 0) {
                    mHandle.post(()->  mAdapter.addData(localNewsBeans));
                    nowPage++;
                    loadMoreDone(localNewsBeans.size());
                } else {
                    mModel.requestDataFromNet(nowPage, PAGE_NUMBER);
                }
            }
        });
    }


    //----------------------------------------------------------------内部调用的方法

    //從網絡添加數據到適配器和本地
    private void addDataFromRequest(boolean isRefreshing, List<NewsBean> requestDataFromNet) {
        isLoadDataFromLocal = false;
        for (NewsBean tempBean : requestDataFromNet) {
            Long nowTime = System.currentTimeMillis();
            Date tempTime = new Date(nowTime);
            tempBean.setRequestTime(tempTime);
        }
        if (isRefreshing) {
            mAdapter.setNewData(requestDataFromNet);
        } else {
            mAdapter.addData(requestDataFromNet);
        }
        mModel.insertData(requestDataFromNet);
    }


    private void resetRefreshFlag() {
        if (isRefreshing) {
            isRefreshing = false;
        }
    }


    /**
     * 加载完成
     *
     * @param count
     */
    private void loadMoreDone(int count) {
        if (count != 0) {
            mView.loadmoreDone(true, count);
        } else {
            mView.loadmoreDone(false, count);
        }

    }

    /**
     * 加载失败
     *
     * @param e
     */
    private void loadMoreFail(Exception e) {
        mView.loadmoreFail(e);
    }


    /**
     * 刷新成功
     *
     * @param count
     */
    private void refreshDone(int count) {
        if (count != 0) {
            mView.refreshDone(true, count);
        } else {
            mView.refreshDone(false, count);
        }


    }

    private void refreshFail(Exception e) {
        mView.refreshFail(e);
    }


    //------------------------------------------------------------------------开放外部调用的方法

    /**
     * 刷新
     */
    @Override
    public void refresh() {
        isRefreshing = true;
        nowPage = 1;
        if (NetUtils.isNetworkConnected(mContext)) {
            isLoadDataFromLocal = false;
            mModel.requestDataFromNet(nowPage, PAGE_NUMBER);
        } else {//无网络 //尝试加载本地数据
            mModel.requestDataFromLocal(nowPage, PAGE_NUMBER);

        }
    }

    /**
     * 加载更多
     */
    @Override
    public void loadMore() {
        if (!isRefreshing) {
            if (isLoadDataFromLocal) {//当前数据从本地加载
                if (nowPage >= MAX_PAGE) {
                    loadMoreDone(0);
                } else {
                    mModel.requestDataFromLocal(nowPage, PAGE_NUMBER);
                }
            } else {
                if (nowPage >= MAX_PAGE) {
                    loadMoreDone(0);
                } else {
                    if (NetUtils.isNetworkConnected(Spone.getApplicationContext())) { //判断当前是否有网络
                        mModel.requestDataFromNet(nowPage, PAGE_NUMBER);
                    } else {
                        mModel.requestDataFromLocal(nowPage, PAGE_NUMBER);
                    }
                }

            }
        }
    }
}
