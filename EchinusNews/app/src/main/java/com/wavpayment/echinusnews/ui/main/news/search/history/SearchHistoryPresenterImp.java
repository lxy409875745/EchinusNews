package com.wavpayment.echinusnews.ui.main.news.search.history;

import android.content.Context;

import com.wavpayment.echinusnews.adapter.SearchHistoryGridViewAdapter;
import com.wavpayment.echinusnews.comment.callback.CallbackConfig;
import com.wavpayment.echinusnews.comment.callback.CallbackManager;
import com.wavpayment.echinusnews.entity.NewsResultBean;
import com.wavpayment.echinusnews.entity.SearchHistoryBean;
import com.wavpayment.echinusnews.weight.completlayout.CompleteLayoutAdapter;

import java.util.List;

/**
 *
 */
public class SearchHistoryPresenterImp implements ISearchHistoryPresenter {

    private Context mContext;
    private ISearchHistoryModel mModel;
    private CompleteLayoutAdapter mAdapter;
    private SearchHistoryGridViewAdapter mGridAdapter;
    private ISearchHistoryView mView;



    public SearchHistoryPresenterImp(Context context, CompleteLayoutAdapter adapter, SearchHistoryGridViewAdapter gridAdapter, ISearchHistoryView view) {
        mContext = context;
        mAdapter = adapter;
        mGridAdapter = gridAdapter;
        mView = view;
        mModel = new SearchHistoryModelImp(mContext);
        initCallback();
    }

    private void initCallback() {
        CallbackManager.getInstance().addCallback(CallbackConfig.RECOMMEND_NEWS,value -> {  //获取推荐新闻
            NewsResultBean resultBean = (NewsResultBean) value;
            if (resultBean!= null){//如果网络有问题就不加载
                if (resultBean.getNewslist()!= null && resultBean.getNewslist().size() >0){
                    mGridAdapter.setNewsData(resultBean.getNewslist());
                }
            }

        });
    }


    /**
     * 添加一条历史记录
     * @param searchWord
     */
    @Override
    public void addHistory(String searchWord) {
        List<SearchHistoryBean> result =  mModel.getHistory(searchWord);
        if (result.size() == 0 ){
            SearchHistoryBean historyBean = new SearchHistoryBean(searchWord);
            mModel.addHistory(historyBean);
        }

    }

    /**
     * 删除所有的历史记录
     */
    @Override
    public void delAllHistory() {
        mModel.delAllHistory();
        mAdapter.removeAll();
    }

    /**
     * 删除单个历史 暂时没完成
     * @param searchWord
     */
    @Override
    public void delHistory(String searchWord) {
       //暂时还不能单个删除
    }

    /**
     * 获取历史记录
     */
    @Override
    public void showHistory() {
        mAdapter.removeAll(); //先清除原先的再添加
       List<SearchHistoryBean> result =  mModel.getHistory();
       for (SearchHistoryBean tempBean:result){
           mAdapter.setData(tempBean.getText());
       }
       if (result != null &&  result.size()!= 0){
           mView.getSearchHistoryDone(true);
       }else {
           mView.getSearchHistoryDone(false);
       }
    }



    @Override
    public void showRecommendNews() {
        mModel.getRecommendNews();
    }


}
