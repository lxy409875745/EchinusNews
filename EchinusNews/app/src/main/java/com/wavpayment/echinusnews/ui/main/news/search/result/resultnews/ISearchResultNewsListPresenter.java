package com.wavpayment.echinusnews.ui.main.news.search.result.resultnews;

public interface ISearchResultNewsListPresenter {
    void setSearchWord(String searchWord);
    void refresh();
    void loadMore();

    void clearNews();//清除所有新闻
}
