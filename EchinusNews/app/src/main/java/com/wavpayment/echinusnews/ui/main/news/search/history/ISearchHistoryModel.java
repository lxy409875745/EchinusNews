package com.wavpayment.echinusnews.ui.main.news.search.history;

import com.wavpayment.echinusnews.entity.SearchHistoryBean;

import java.util.List;

public interface ISearchHistoryModel {


    /**
     * 获取推荐新闻 （随机的社会新闻）
     */
    void getRecommendNews();
    /** 添加历史记录
     * @param historyBean
     */
    void addHistory(SearchHistoryBean historyBean);

    /**
     * 删除所有历史记录
     */
    void delAllHistory();

    /**
     * 删除历史记录
     */
    void delHistory(String historyBean);

    /**
     * 获取history
     */
    List<SearchHistoryBean> getHistory();

    /** 根据条件来查询历史记录
     * @param keywrod
     */
    List<SearchHistoryBean> getHistory(String keywrod);
}
