package com.wavpayment.echinusnews.ui.luncher.welcome;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.wavpayment.echinusnews.R;
import com.wavpayment.echinusnews.comment.callback.CallbackConfig;
import com.wavpayment.echinusnews.comment.callback.CallbackManager;
import com.wavpayment.echinusnews.utils.ResourceUtils;
import com.wavpayment.echinusnews.utils.StatusBarCompat;

/**
 * Created by Administrator on 2018/5/11.
 */

public class WelcomeActivity extends AppIntro {
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarCompat.translucentStatusBar(this);
        initAppIntor();
    }

    private void initAppIntor() {

        int color_1 = ResourceUtils.getColor(this,R.color.color_orange_500);
        int color_2 = ResourceUtils.getColor(this,R.color.color_green_500);
        int color_3 = ResourceUtils.getColor(this,R.color.app_color);

        addSlide(AppIntroFragment.newInstance("","","","",R.drawable.fragmentation_ic_stack,color_1,color_3,color_3));
        addSlide(AppIntroFragment.newInstance("","","","",R.drawable.fragmentation_ic_stack,color_2,color_2,color_2));
        addSlide(AppIntroFragment.newInstance("","","","",R.drawable.fragmentation_ic_stack,color_3,color_1,color_1));


        setSeparatorColor(Color.parseColor("#2196F3"));

        showSkipButton(true);
        setProgressButtonEnabled(true);
    }

    @Override
    public void onSkipPressed() {
        finish();
        CallbackManager.getInstance().getCallback(CallbackConfig.WELCOME_END).excuteCallback(null);

    }

    @Override
    public void onDonePressed() {
        finish();
        CallbackManager.getInstance().getCallback(CallbackConfig.WELCOME_END).excuteCallback(null);
    }

    @Override
    public void onBackPressed() {

    }
}
