package com.wavpayment.echinusnews.comment.callback;

/**
 * Created by Administrator on 2018/5/11.
 */

public  enum CallbackConfig {
    WELCOME_END, //欢迎界面结束
    RECOMMEND_NEWS,//推荐的新闻
    SEARCH_HISTORY,//历史记录

    WECHAT_NEWS_NET,//从网络获取的微信文章
    WECHAT_NEWS_LOCAL//从本地获取的微信文章
}
