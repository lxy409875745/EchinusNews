package com.wavpayment.echinusnews.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.wavpayment.echinusnews.comment.network.URLManager;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Keep;

/**
 * Created by Administrator on 2018/5/12.
 */
@Keep
@Entity
public class NewsTypeBean implements Parcelable{
    private String title;
    private String url;
    private boolean isShowing;//是否显示

    public NewsTypeBean(String title, String url, boolean isShowing) {
        this.title = title;
        this.url = url;
        this.isShowing = isShowing;
    }

    protected NewsTypeBean(Parcel in) {
        title = in.readString();
        url = in.readString();
        isShowing = in.readByte() != 0;
    }

    public static final Creator<NewsTypeBean> CREATOR = new Creator<NewsTypeBean>() {
        @Override
        public NewsTypeBean createFromParcel(Parcel in) {
            return new NewsTypeBean(in);
        }

        @Override
        public NewsTypeBean[] newArray(int size) {
            return new NewsTypeBean[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean getIsShowing() {
        return isShowing;
    }

    public void setIsShowing(boolean showing) {
        isShowing = showing;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(url);
        dest.writeByte((byte) (isShowing ? 1 : 0));
    }
}
