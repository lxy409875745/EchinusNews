package com.wavpayment.echinusnews.ui.main.news;

import android.content.Context;

import com.wavpayment.echinusnews.adapter.NewsPagerAdapter;
import com.wavpayment.echinusnews.entity.NewsTypeBean;

import java.util.List;

/**
 * Created by Administrator on 2018/5/12.
 */

public class NewsPresenter {
    private Context mContext;
    private NewsPagerAdapter mAdapter;
    private NewsModel mNewsModel;

    /**
     * @param mContext
     * @param mAdapter
     */
    public NewsPresenter(Context mContext, NewsPagerAdapter mAdapter) {
        this.mContext = mContext;
        this.mAdapter = mAdapter;
        mNewsModel = new NewsModel(mContext);
        initTabType();


        List<NewsTypeBean> data = mNewsModel.getNewsTypes();
        setNewsType(data);
    }


    private void initTabType() {
        mNewsModel.initDefualNewsTypes();
    }

    private void setNewsType(List<NewsTypeBean> data) {
        mAdapter.addData(data);
    }


}
